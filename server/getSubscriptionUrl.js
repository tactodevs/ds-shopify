const getSubscriptionUrl = async (ctx, accessToken, shop) => {
  const query = JSON.stringify({
    query: `mutation {
      appSubscriptionCreate(
          name: "Super Duper Plan"
          returnUrl: "${process.env.APP_HOST}/orders"
          test:${process.env.NODE_ENV !== 'production'}
          lineItems: [
          {
            plan: {
              appUsagePricingDetails: {
                  cappedAmount: { amount: 500, currencyCode: USD }
                  terms: "$1 per delivery"
              }
            }
          }
          ]
        ) {
            userErrors {
              field
              message
            }
            confirmationUrl
            appSubscription {
              id
            }
        }
    }`,
  });

  const response = await fetch(`https://${shop}/admin/api/2019-07/graphql.json`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'X-Shopify-Access-Token': accessToken,
    },
    body: query,
  });

  const responseJson = await response.json();
  const { confirmationUrl } = responseJson.data.appSubscriptionCreate;
  return ctx.redirect(confirmationUrl);
};

module.exports = getSubscriptionUrl;
