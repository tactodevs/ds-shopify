const { gql } = require('apollo-server-koa');

const typeDefs = gql`
  type Address {
    name: String
    phone: String
    address1: String
    address2: String
    city: String
    province: String
    provinceCode: String
    countryCode: String
    zip: String
    country: String
    createdAt: String
  }

  input DSAddress {
    street: String!
    street2: String
    city: String!
    state: String
    zipcode: String
    country: String
    apartmentNumber: String
  }

  input Contact {
    name: String!
    phone: String!
  }

  input DSPickup {
    name: String = ""
    contact: Contact
    pickupInstructions: String = ""
    address: DSAddress!
  }

  type Order {
    id: Int!
    shopifyOrderId: String!
    orderName: String!
    estimatedCost: Float!
    actualCost: Float
    dsOrderId: String
    dspName: String
    dsTrackingUrl: String
    dsTrackingNumber: String
    deliveredAt: String
    dsOrderStatus: String
    createdAt: String
    updatedAt: String
    myshopifyDomain: String
    shippingAddress: Address
    location: Address
  }
  input Package {
    name: String!
    weight: Float!
    quantity: Float!
  }
  type Estimate {
    customerName: String!
    amount: String!
    estimatedDeliveryTime: String!
    _id: String!
  }
  type OrderData {
    rows: [Order]
    count: Int
  }
  input EstimateInput {
    isSpirit: Boolean!
    dropOffTime: String!
    signature: String!
    isBeerOrWine: Boolean!
    isTobacco: Boolean!
    isFragile: Boolean!
    hasPerishableItems: Boolean!
  }
  type Shop {
    id: Int!
    shopId: String!
    myshopifyDomain: String!
    name: String!
    email: String
    contactEmail: String
    maximumCharge: Float
    addOnCharge: Float
    fixedCharge: Float
    chargingMethod: String
    appEnabled: Boolean!
    widgetEnabled: Boolean
    deliveryTitle: String
    apiKey: String
  }
  input ShopInput {
    fixedCharge: Float
    chargingMethod: String
    addOnCharge: Float
    maximumCharge: Float
    appEnabled: Boolean
    widgetEnabled: Boolean
    deliveryTitle: String
    apiKey: String
  }
  type Query {
    orders(limit: Int, offset: Int, searchTerm: String = ""): OrderData
    shop(myshopifyDomain: String!): Shop
    order(shopifyOrderId: String!): Order
    getEstimate(shopifyOrderId: String!, input: EstimateInput): Estimate
  }
  type Mutation {
    getEstimations(orderIds: [String]!): Order
    createDelivery(estimateId: String!, shopifyOrderId: String!): Order
    cancelDelivery(dsOrderId: String!, shopifyOrderId: String!, checked: Boolean!, cancelReason: String!): Order
    updateShop(myshopifyDomain: String!, input: ShopInput): Shop
  }
`;
module.exports = typeDefs;
