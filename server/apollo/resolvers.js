const { orders, shop, updateShop, order } = require('../models');
const { getEstimate, createDelivery, cancelDelivery } = require('../models/ds-api');
const resolvers = {
  Query: {
    orders,
    order,
    shop,
    getEstimate,
  },
  Mutation: {
    cancelDelivery,
    createDelivery,
    updateShop,
  },
};

module.exports = resolvers;
