const init = async () => {
  const { initDatabase } = require('./models/database');
  await initDatabase();
  require('./app');
};
init();
