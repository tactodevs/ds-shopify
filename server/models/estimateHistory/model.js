const { DataTypes } = require('sequelize');

module.exports = database => {
  const EstimateHistory = database.define('estimateHistory', {
    pickupAddress: {
      type: DataTypes.JSONB,
      allowNull: false,
    },
    deliveryAddress: {
      type: DataTypes.JSONB,
      allowNull: false,
    },
    deliveryTime: {
      type: DataTypes.DATE,
    },
    deliveryCharge: {
      type: DataTypes.DECIMAL,
    },
  });

  return EstimateHistory;
};
