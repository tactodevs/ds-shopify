const { models } = require('../database');

const addEstimateHistory = async payload => {
  const history = await models.EstimateHistory.upsert(payload);
  return history;
};
module.exports = { addEstimateHistory };
