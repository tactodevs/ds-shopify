const { orders, order, getEstimations } = require('./order');
const { shop, updateShop, addShop, getShop } = require('./shop');
const { addEstimateHistory } = require('./estimateHistory');

module.exports = { updateShop, shop, addEstimateHistory, getShop, addShop, orders, order, getEstimations };
