const { DataTypes } = require('sequelize');

module.exports = database => {
  const ShippingAddress = database.define('shippingAddress', {
    name: {
      type: DataTypes.STRING,
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    address1: {
      type: DataTypes.STRING,
    },
    address2: {
      type: DataTypes.STRING,
    },
    city: {
      type: DataTypes.STRING,
    },
    province: {
      type: DataTypes.STRING,
    },
    provinceCode: {
      type: DataTypes.STRING,
    },
    country: {
      type: DataTypes.STRING,
    },
    countryCode: {
      type: DataTypes.STRING,
    },
    zip: {
      type: DataTypes.STRING,
    },
  });
  return ShippingAddress;
};
