const { DataTypes } = require('sequelize');

const values = ['ADD_ON', 'FIXED'];
module.exports = database => {
  const Shop = database.define('shop', {
    shopId: {
      type: DataTypes.STRING,
      unique: true,
    },
    myshopifyDomain: {
      type: DataTypes.STRING,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
    },
    contactEmail: {
      type: DataTypes.STRING,
    },
    email: {
      type: DataTypes.STRING,
    },
    maximumCharge: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: 1,
    },
    fixedCharge: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: 1,
    },
    addOnCharge: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: 0,
    },
    appEnabled: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
    widgetEnabled: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
    accessToken: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    chargeId: {
      type: DataTypes.STRING,
    },
    chargingMethod: {
      type: DataTypes.ENUM,
      values,
      allowNull: false,
      defaultValue: values[0],
    },
    deliveryTitle: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'Same day delivery',
    },
    apiKey: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
  });

  Shop.associate = models => {
    Shop.hasMany(models.Order, {
      foreignKey: { name: 'myshopifyDomain', allowNull: false },
      sourceKey: 'myshopifyDomain',
      onDelete: 'CASCADE',
    });

    models.Order.belongsTo(Shop, {
      foreignKey: { name: 'myshopifyDomain', allowNull: false },
      targetKey: 'myshopifyDomain',
    });

    Shop.hasMany(models.EstimateHistory, {
      foreignKey: { name: 'myshopifyDomain', allowNull: false },
      onDelete: 'CASCADE',
      sourceKey: 'myshopifyDomain',
    });
    models.EstimateHistory.belongsTo(Shop, {
      foreignKey: { name: 'myshopifyDomain', allowNull: false },
      targetKey: 'myshopifyDomain',
    });

    Shop.hasMany(models.Location, {
      foreignKey: { name: 'myshopifyDomain', allowNull: false },
      onDelete: 'CASCADE',
      sourceKey: 'myshopifyDomain',
    });

    models.Location.belongsTo(Shop, {
      foreignKey: { name: 'myshopifyDomain', allowNull: false },
      targetKey: 'myshopifyDomain',
    });
  };
  return Shop;
};
