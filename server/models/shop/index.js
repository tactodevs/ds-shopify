const ShopifyAPIClient = require('shopify-api-node');
const fs = require('fs');
const { isNull } = require('lodash');
const { models } = require('../database');
const logger = require('../../logger');

const shop = async (_, args) => {
  try {
    const { myshopifyDomain } = args;
    const data = await models.Shop.findOne({ where: { myshopifyDomain }, raw: true });
    return data;
  } catch (error) {
    logger.error(error);
    return error;
  }
};

const addWidget = async shopifyClient => {
  const widgetFileData = fs.readFileSync('./widget/index.html', { encoding: 'utf8', flag: 'r' });
  const themeList = await shopifyClient.theme.list();
  const themeSelected = themeList.filter(t => t.role === 'main');
  const themeID = themeSelected[0].id;
  shopifyClient.asset.create(themeID, { key: 'snippets/foxtrot.liquid', value: widgetFileData });
  const cartFile = await shopifyClient.asset.get(themeID, {
    asset: { key: 'sections/cart-template.liquid' },
  });
  const cartValue = cartFile.value;
  if (
    !cartValue.includes("<!-- Foxtrot same day delivery -->{% include 'foxtrot' %} <!-- Foxtrot same day delivery -->")
  ) {
    const indexOfTable = cartValue.indexOf('</table>');
    // Foxtrot Widget
    const widgetCode = "<!-- Foxtrot same day delivery -->{% include 'foxtrot' %} <!-- Foxtrot same day delivery -->";
    const newLiquid = cartValue.slice(0, indexOfTable + 8) + widgetCode + cartValue.slice(indexOfTable + 8);
    shopifyClient.asset.update(themeID, {
      key: 'sections/cart-template.liquid',
      value: newLiquid,
    });
  }
};

const removeWidget = async shopifyClient => {
  const themeList = await shopifyClient.theme.list();
  const themeSelected = themeList.filter(t => t.role === 'main');
  const themeID = themeSelected[0].id;

  const cartFile = await shopifyClient.asset.get(themeID, {
    asset: { key: 'sections/cart-template.liquid' },
  });
  const cartValue = cartFile.value;
  if (
    cartValue.includes("<!-- Foxtrot same day delivery -->{% include 'foxtrot' %} <!-- Foxtrot same day delivery -->")
  ) {
    const widgetCode = "<!-- Foxtrot same day delivery -->{% include 'foxtrot' %} <!-- Foxtrot same day delivery -->";
    const newLiquid = cartValue.replace(widgetCode, '');
    shopifyClient.asset.update(themeID, {
      key: 'sections/cart-template.liquid',
      value: newLiquid,
    });
  }
  await shopifyClient.asset.delete(themeID, { asset: { key: 'snippets/foxtrot.liquid' } });
};
const updateWidgetTitle = async (shopifyClient, deliveryTitle) => {
  logger.info('deliveryTitle');
  logger.info(deliveryTitle);
  const themeList = await shopifyClient.theme.list();
  const themeSelected = themeList.filter(t => t.role === 'main');
  const themeID = themeSelected[0].id;
  const cartFile = await shopifyClient.asset.get(themeID, {
    asset: { key: 'snippets/foxtrot.liquid' },
  });
  const cartValue = cartFile.value;
  const startIndex = cartValue.indexOf('class="btn">');
  const endTable = cartValue.indexOf('</button>');
  const newLiquid = cartValue.slice(0, startIndex + 12) + deliveryTitle + cartValue.slice(endTable);
  shopifyClient.asset.update(themeID, {
    key: 'snippets/foxtrot.liquid',
    value: newLiquid,
  });
};
const updateAPIKey = async (shopifyClient, apiKey) => {
  const themeList = await shopifyClient.theme.list();
  const themeSelected = themeList.filter(t => t.role === 'main');
  const themeID = themeSelected[0].id;
  const cartFile = await shopifyClient.asset.get(themeID, {
    asset: { key: 'snippets/foxtrot.liquid' },
  });
  const cartValue = cartFile.value;
  const startIndex = cartValue.indexOf('src="https://maps.googleapis.com/maps/api/js?key=');
  const endTable = cartValue.indexOf('&libraries=places&callback=initPlaces"');

  const deleteElement = cartValue.slice(0, startIndex + 49) + cartValue.slice(endTable);
  const start = deleteElement.indexOf('src="https://maps.googleapis.com/maps/api/js?key=');
  const end = deleteElement.indexOf('&libraries=places&callback=initPlaces"');
  const newLiquid = deleteElement.slice(0, start + 49) + apiKey + deleteElement.slice(end);
  shopifyClient.asset.update(themeID, {
    key: 'snippets/foxtrot.liquid',
    value: newLiquid,
  });
  logger.info('updated API');
};
const updateShop = async (_, args) => {
  try {
    const { input, myshopifyDomain } = args;
    await models.Shop.update(input, { where: { myshopifyDomain } });
    const data = await models.Shop.findOne({ where: { myshopifyDomain }, raw: true });
    const shopifyClient = new ShopifyAPIClient({ shopName: myshopifyDomain, accessToken: data.accessToken });
    if (args.input.widgetEnabled === true) {
      addWidget(shopifyClient);
    } else if (args.input.widgetEnabled === false) {
      removeWidget(shopifyClient);
    } else {
      updateWidgetTitle(shopifyClient, args.input.deliveryTitle);
    }
    if (!isNull(input.apiKey)) {
      updateAPIKey(shopifyClient, input.apiKey);
    }

    return data;
  } catch (error) {
    logger.error(error);
    return error;
  }
};

const getShop = async myshopifyDomain => {
  const data = await models.Shop.findOne({ where: { myshopifyDomain }, raw: true });
  return data;
};

const addShop = async payload => {
  const shop = await models.Shop.upsert(payload, { include: [models.Location] });
  return shop;
};

module.exports = { getShop, updateShop, addShop, shop, removeWidget };
