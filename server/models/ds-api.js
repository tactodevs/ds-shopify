const axios = require('axios');
const isEmpty = require('lodash/isEmpty');
const moment = require('moment');
const { ApolloError } = require('apollo-server-koa');
const ShopifyAPIClient = require('shopify-api-node');
const logger = require('../logger');
const { addEstimateHistory, getShop } = require('../models');
const { token } = require('morgan');
const { indexOf } = require('lodash');
const { DS_HOST, DS_API_KEY } = process.env;
const headers = { 'Content-Type': 'application/json', 'x-api-key': DS_API_KEY };
const dsp=['DoorDash', 'Postmates'];

// Pick an estimate with the earliest delivery time and with the least cost.
const desiredEstimate = estimates => {
  const todaysDate = moment(Date.now()).format('YYYY-MM-DD');
// checking for only those DSPs who we've partnered with.
  const canDeliver= estimates.filter(({provider})=>{
    return dsp.includes(provider);
  });

  // Get the estimate of service which can delivery today.
  // const canDeliveryToday = estimates.filter(({ estimatedDeliveryTime }) => {
  //   const deliveryTime = moment(estimatedDeliveryTime).format('YYYY-MM-DD');
  //   return deliveryTime === todaysDate;
  // });
  if (!isEmpty(canDeliver)) {
    // Get the estimate which charges the least amount.
    const mapped = canDeliver.reduce((first, second) => {
      const final = first.amount < second.amount ? first : second;
      return final;
    });
    return mapped;
  }
  return [];
};

// Get estimate api call.

const getEstimate = async (_, { shopifyOrderId, input }, context) => {
  const { dropOffTime, ...rest } = input;
  //giving dsp a three day buffer to pick up order
  const endsAt = parseFloat(dropOffTime) + 259200000;
  try {
    const { models, shop, accessToken } = context;
    const shopifyClient = new ShopifyAPIClient({ shopName: shop, accessToken });

    let shopifyOrder;
    try {
      shopifyOrder = await shopifyClient.order.get(shopifyOrderId);
    } catch (error) {
      throw new ApolloError(error.message);
    }
    const { total_weight, line_items, total_price } = shopifyOrder;

    const order = await models.Order.findOne({
      where: { shopifyOrderId },
      include: [models.ShippingAddress, models.Location],
    });

    if (isEmpty(order)) {
      logger.error('Order does not exist.');
      return 'Order does not exists';
    }

    // Convert grams to lbs
    const weight = total_weight / 453.592;
    const { shippingAddress, locationId } = order;

    const pickupAddress = await models.Location.findOne({ where: { locationId } });

    const payload = {
      department: 'foxtrot',
      packages: [{ name: 'custom', weight, quantity: line_items.length }],
      orderValue: parseFloat(total_price),
      deliveryInstructions: '',
      ...rest,
      pickupTime: null,
      dropoffTime: { startsAt: dropOffTime, endsAt },
      pickupInstructions: '',
      deliveryContact: {
        name: shippingAddress.name,
        phone: '+1 412-312-3123',
      },
      deliveryAddress: {
        street: shippingAddress.address1,
        street2: shippingAddress.address2,
        city: shippingAddress.city,
        state: shippingAddress.province,
        country: shippingAddress.countryCode,
        zipcode: shippingAddress.zip,
      },

      pickup: {
        name: pickupAddress.name || 'Foxtrot',
        contact: {
          name: pickupAddress.name || 'Foxtrot',
          phone: '+1 412-312-3123',
        },
        address: {
          street: pickupAddress.address1,
          street2: pickupAddress.address2,
          city: pickupAddress.city,
          state: pickupAddress.province,
          country: pickupAddress.country,
          zipcode: pickupAddress.zip,
        },
        departments: [{ name: 'foxtrot' }],
        brandExternalId: shop,
      },
    };

    const url = `${DS_HOST}/api/v2/order/estimates`;
    const {
      data: { estimates },
    } = await axios({ method: 'POST', url, data: payload, headers });
    if (!isEmpty(estimates)) {
      const estimate = desiredEstimate(estimates);
      if (!isEmpty(estimate)) {
        const { amount, _id, estimatedDeliveryTime } = estimate;
        const amountWithMarkup = amount / 100 + 1;
        return { amount: amountWithMarkup, _id, estimatedDeliveryTime };
      }
    }
  } catch (error) {
    logger.error(error);
    return false;
  }
};

// Get the assurance on checkout pay of shopify.
const getDeliveryAssurance = async (payload, shop, currency) => {
  const { maximumCharge, addOnCharge, fixedCharge, chargingMethod, appEnabled, deliveryTitle } = await getShop(shop);

  /**
   * If app is disabled don't bother to go ahead.
   */
  if (isEmpty(shop) || !appEnabled) {
    return [];
  }
  try {
    const url = `${DS_HOST}/api/v2/deliveryAssurance`;

    const { data } = await axios({ method: 'POST', url, data: payload, headers });
    const estimates = data.dsp && data.dsp.estimates;
    if (!isEmpty(estimates)) {
      const estimatedAmount = desiredEstimate(estimates);
      if (isEmpty(estimatedAmount)) {
        return [];
      }
      // Max amount and addOnAmount is taken from owner in dollars.
      const estimatedAmountInDollars = estimatedAmount.amount / 100;

      let finalCharge = '';

      /**
       * If charing method is ADD_ON and estimatedAmount + addOnCharge is < maximumCharge
       * add the estimatedAmount aiwth addOnChage and show it to the customer else show nothing.
       */

      if (
        chargingMethod === 'ADD_ON' &&
        estimatedAmountInDollars + parseFloat(addOnCharge) < parseFloat(maximumCharge)
      ) {
        finalCharge = (parseFloat(estimatedAmountInDollars) + parseFloat(addOnCharge)).toFixed(2);
      }

      /**
       * If the charging method is FIXED then check if estimatedAmount < fixedCharge.
       */

      if (chargingMethod === 'FIXED' && estimatedAmountInDollars < parseFloat(fixedCharge)) {
        finalCharge = parseFloat(fixedCharge);
      }

      // Add estimate to log to debug incase of any issues.
      const historyPayload = {
        myshopifyDomain: shop,
        pickupAddress: payload.pickupAddress,
        deliveryAddress: payload.deliveryAddress,
        deliveryCharge: parseFloat(finalCharge),
        estimatedAmountInDollars,
        deliveryTime: estimatedAmount.estimatedDeliveryTime,
      };
      addEstimateHistory(historyPayload);
      if (finalCharge === '') {
        logger.info('No final charge.');
        return [];
      }
      logger.info(`The final charge is - ${finalCharge}`);
      logger.info(`DSP ${estimatedAmount.provider}`);
      // We are going to use this address to set our default pickup location while creating order.
      // This is an hack there has to be a better way to do it.
      const { street = '', street2 = '', city = '', state = '', zipcode = '', country = '' } = payload.pickupAddress;
      const deliveryTime = moment(desiredEstimate.estimatedDeliveryTime).format('hh:mm a');
      const deliveryDate= moment(desiredEstimate.estimatedDeliveryTime).format('YYYY-MM-DD');
      return {
        rates: [
          {
            service_name: deliveryTitle,
            service_code: `${street}^${street2}^${city}^${state}^${country}^${zipcode}`,
            total_price: finalCharge * 100,
            phone_required: true,
            description: `Order will be delivered on ${deliveryDate} at around ${deliveryTime}.`,
            currency,
          },
        ],
      };
    }
    return [];
  } catch (error) {
    logger.error(error);
    return false;
  }
};

// Create delivery from the estimate.
const createDelivery = async (_, args, { models, shopifyClient }) => {
  try {
    const { estimateId, shopifyOrderId } = args;
    const { Order } = models;

    const url = `${DS_HOST}/api/v2/order/createOrderFromEstimate/${estimateId}`;
    const {
      data: { provider, amount, status, trackingUrl: dsTrackingUrl, trackingNumber: dsTrackingNumber, orderId },
    } = await axios({ method: 'POST', url, headers });

    // Add $1 per delivery in estimated cost.
    const actualCost = parseFloat(amount) / 100 + 1;

    const payload = {
      dsOrderStatus: status,
      dsOrderId: orderId,
      actualCost,
      dspName: provider,
      dsTrackingUrl,
      dsTrackingNumber,
    };
    await Order.update(payload, { where: { shopifyOrderId } });

    const query = { where: { shopifyOrderId }, include: [models.ShippingAddress, models.Location] };
    const order = await Order.findOne(query);

    const fulfullmentTrackingPayload = {
      location_id: order.locationId,
      tracking_number: dsTrackingNumber,
      tracking_urls: [dsTrackingUrl],
      notify_customer: true,
    };

    // Mark order as fulfilled and add tracking number and tracking url to the shopify order.
    const fullfilResponse = await shopifyClient.fulfillment.create(shopifyOrderId, fulfullmentTrackingPayload);
    const { id } = fullfilResponse;
    const newPayload = {
      dsOrderStatus: status,
      dsOrderId: orderId,
      actualCost,
      dspName: provider,
      dsTrackingUrl,
      dsTrackingNumber,
      fulfillmentId: id,
    };
    await Order.update(newPayload, { where: { shopifyOrderId } });
    const newQuery = { where: { shopifyOrderId }, include: [models.ShippingAddress, models.Location] };
    const newOrder = await Order.findOne(newQuery);

    return newOrder;
  } catch (error) {
    logger.error(error);
    throw new ApolloError(error);
  }
};
const cancelDelivery = async (_, args, { models }) => {
  try {
    const { dsOrderId, shopifyOrderId, checked, cancelReason } = args;
    const { Order } = models;

    const url = `${DS_HOST}/api/v2/order/${dsOrderId}`;
    const {
      data: { provider, amount, status, trackingUrl: dsTrackingUrl, trackingNumber: dsTrackingNumber, orderId },
    } = await axios({ method: 'DELETE', url, headers });
    const actualCost = parseFloat(amount) / 100 + 1;

    const payload = {
      dsOrderStatus: status,
      dsOrderId: orderId,
      actualCost,
      dspName: provider,
      dsTrackingUrl,
      dsTrackingNumber,
    };
    await Order.update(payload, { where: { shopifyOrderId } });
    const query = { where: { shopifyOrderId }, include: [models.ShippingAddress, models.Location] };
    const order = await Order.findOne(query);
    const { accessToken } = await getShop(order.myshopifyDomain);
    console.log(accessToken);
    const shopifyClient = new ShopifyAPIClient({ shopName: order.myshopifyDomain, accessToken });

    console.log(shopifyOrderId, order.fulfillmentId);
    const d = {};
    const data = { reason: cancelReason };
    await shopifyClient.fulfillment.cancel(shopifyOrderId, order.fulfillmentId, d);
    await shopifyClient.order.cancel(shopifyOrderId, data);
    console.log(checked);
    if (checked) {
      // restock
      const lineItems = await shopifyClient.order.get(shopifyOrderId);

      const { line_items } = lineItems;
      logger.info(`line items${line_items}`);
      const variant_ids = line_items.map(v => {
        const vid = v.variant_id;
        return vid;
      });
      logger.info(`variant ids${variant_ids}`);
      const quantities = line_items.map(q => {
        const { quantity } = q;
        return quantity;
      });
      logger.info(`quantity${quantities}`);
      const loc = order.locationId;
      logger.info(loc);
      const inventory_ids = await Promise.all(
        variant_ids.map(async varId => {
          const { inventory_item_id } = await shopifyClient.productVariant.get(varId);
          return inventory_item_id;
        }),
      );
      console.log(inventory_ids);
      const restock = await Promise.all(
        inventory_ids.map(async (value, index) => {
          const q = await quantities[index];

          await shopifyClient.inventoryLevel.adjust({
            inventory_item_id: value,
            location_id: loc,
            available_adjustment: q,
          });
          return 1;
        }),
      );
      console.log(restock);
    }
    return order;
  } catch (error) {
    console.log(error);
    throw new ApolloError(error);
  }
};

const createBrandForShop = async shop => {
  try {
    const url = `${DS_HOST}/api/v2/brand`;
    const { address1, address2, latitude, longitude, myshopify_domain, name, province, zip, country, city } = shop;
    /**
     * Here we are using shopify store domain as a unique brand id which will always we unique
     * This same domain id will be used for trasanction as a brandExternal id.
     */
    const payload = {
      name,
      brandExternalId: myshopify_domain,
      address: {
        latitude,
        longitude,
        city,
        country,
        zipcode: zip,
        state: province,
        street: address1,
        street2: address2,
      },
    };
    await axios({ method: 'POST', url, headers, data: payload });
    logger.info('brand created successfully.');
  } catch (error) {
    logger.error(error);
  }
};
module.exports = { createBrandForShop, getEstimate, createDelivery, getDeliveryAssurance, cancelDelivery };
