const isEmpty = require('lodash/isEmpty');
const { AuthenticationError } = require('apollo-server-koa');
const Sequelize = require('sequelize');
const Shop = require('../shop/model');
const logger = require('../../logger');

const {
  models: { Order, Location, ShippingAddress },
} = require('./../database');

const orders = async (_, args, context) => {
  const { offset = 0, limit = 25, searchTerm = '' } = args;
  const { shop: myshopifyDomain, accessToken, models } = context;
  if (isEmpty(accessToken)) {
    throw new AuthenticationError('Unauthorized access.');
  }
  const query = {
    where: {
      myshopifyDomain,
      orderName: {
        [Sequelize.Op.iLike]: `%${searchTerm}%`,
      },
    },
    include: [ShippingAddress, Location],
    limit,
    offset: offset * limit,
  };
  const { count, rows } = await models.Order.findAndCountAll(query);
  return { count, rows };
};

const order = async (_, args) => {
  const { shopifyOrderId } = args;
  const data = await Order.findOne({
    where: { shopifyOrderId },
    include: [ShippingAddress, Location],
  });
  return data;
};

const updateOrder = async (dsOrderId, dsOrderStatus) => {
  try {
    await Order.update({ dsOrderStatus }, { where: { dsOrderId } });
    const order = await Order.findOne({ where: { dsOrderId } });
    const shop = await Shop.findOne({ where: { myshopifyDomain: order.myshopifyDomain } });
    return { order, shop };
  } catch (error) {
    logger.error(error);
    return {};
  }
};
const updateCanceledOrder = async (dsOrderStatus, shopifyOrderId) => {
  try {
    await Order.update({ dsOrderStatus }, { where: { shopifyOrderId } });
    const order = await Order.findOne({ where: { shopifyOrderId } });
    const shop = await Shop.findOne({ where: { myshopifyDomain: order.myshopifyDomain } });
    return { order, shop };
  } catch (error) {
    logger.error(error);
    return {};
  }
};

const addOrder = async payload => {
  try {
    await Order.create(payload, { include: [ShippingAddress] });
  } catch (error) {
    logger.error(error);
  }
};

module.exports = { addOrder, orders, order, updateOrder, updateCanceledOrder };
