const { DataTypes } = require('sequelize');

const values = [
  'ORDER_FAILED',
  'PENDING_RETRY',
  'ORDER_DELAYED',
  'ORDER_ASSIGNED',
  'PICKUP_STARTED',
  'ORDER_RETURNED',
  'PENDING_RELEASE',
  'ORDER_CONFIRMED',
  'ORDER_DELIVERED',
  'ORDER_CANCELLED',
  'ERROR_EXCEPTION',
  'REQUEST_RECEIVED',
  'PENDING_DISPATCH',
  'ESTIMATES_FAILED',
  'ORDER_DISPATCHED',
  'PICKUP_COMPLETED',
  'PICKUP_EXCEPTION',
  'OUT_FOR_DELIVERY',
  'ORDER_UNASSIGNED',
  'ORDER_REDELIVERY',
  'ESTIMATES_RECEIVED',
  'ORDER_UNDELIVERABLE',
];

module.exports = database => {
  const Order = database.define('order', {
    shopifyOrderId: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    orderName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    estimatedCost: {
      type: DataTypes.DECIMAL,
      allowNull: false,
    },
    actualCost: {
      type: DataTypes.FLOAT,
    },
    dsOrderId: {
      type: DataTypes.STRING,
      unique: true,
    },
    dsTrackingUrl: {
      type: DataTypes.TEXT,
    },
    dsTrackingNumber: {
      type: DataTypes.STRING,
    },
    dspName: {
      type: DataTypes.TEXT,
    },
    deliveredAt: {
      type: DataTypes.TIME,
    },
    dsOrderStatus: {
      type: DataTypes.ENUM,
      values,
    },
    totalCost: {
      type: DataTypes.DECIMAL,
      allowNull: false,
    },
    currency: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    fulfillmentId: {
      type: DataTypes.STRING,
    },
  });

  Order.associate = models => {
    const forgeinKey = {
      foreignKey: { name: 'shopifyOrderId', allowNull: false },
      sourceKey: 'shopifyOrderId',
      onDelete: 'CASCADE',
    };
    const forgeinKey1 = {
      foreignKey: { name: 'shopifyOrderId', allowNull: false },
      targetKey: 'shopifyOrderId',
    };

    Order.hasOne(models.ShippingAddress, forgeinKey);
    models.ShippingAddress.belongsTo(Order, forgeinKey1);

    models.Location.hasMany(models.Order, {
      foreignKey: { name: 'locationId', allowNull: false },
      sourceKey: 'locationId',
      onDelete: 'CASCADE',
    });

    models.Order.belongsTo(models.Location, {
      foreignKey: { name: 'locationId', allowNull: false },
      targetKey: 'locationId',
    });
  };
  return Order;
};
