/* eslint-disable no-param-reassign */

const fs = require('fs');
const pick = require('lodash/pick');
const ShopifyAPIClient = require('shopify-api-node');
const logger = require('../logger');
const { addShop } = require('./shop');
const { createBrandForShop } = require('./ds-api');
const { addLocations } = require('./location');

const initShopifyClient = (shopName, accessToken) => {
  try {
    logger.info('initShopifyClient');
    initShop(shopName, accessToken);
  } catch (error) {
    logger.error(error);
  }
};

const initShop = async (shop, accessToken) => {
  try {
    const shopifyClient = new ShopifyAPIClient({ shopName: shop, accessToken });
    // get theme list
    addWidget(shopifyClient);

    // Get the shop details and all the store(locations of that shop.)
    const shopDetails = await shopifyClient.shop.get();
    const locations = await shopifyClient.location.list();

    const { id, myshopify_domain, name, customer_email } = shopDetails;

    const mappedLocations = locations.map(value => {
      value.countryCode = value.country_code;
      value.provinceCode = value.province_code;
      value.locationId = value.id;
      value.myshopifyDomain = myshopify_domain;
      return pick(value, [
        'myshopifyDomain',
        'locationId',
        'countryCode',
        'provinceCode',
        'name',
        'address1',
        'address2',
        'city',
        'zip',
        'province',
        'country',
        'phone',
      ]);
    });

    const payload = {
      shopId: id,
      myshopifyDomain: myshopify_domain,
      name,
      accessToken,
      contactEmail: customer_email,
    };

    await addShop(payload);
    await addLocations(mappedLocations);
    await createBrandForShop(shopDetails);
  } catch (error) {
    logger.error(error);
  }
};
const addWidget = async shopifyClient => {
  const widgetFileData = fs.readFileSync('./widget/index.html', { encoding: 'utf8', flag: 'r' });
  const themeList = await shopifyClient.theme.list();
  // filter to main theme ID
  const themeSelected = themeList.filter(t => t.role === 'main');
  // get theme ID

  const themeID = themeSelected[0].id;
  // create an asset
  shopifyClient.asset.create(themeID, { key: 'snippets/foxtrot.liquid', value: widgetFileData });
  // get cart-template.liquid file
  const cartFile = await shopifyClient.asset.get(themeID, {
    asset: { key: 'sections/cart-template.liquid' },
  });
  const cartValue = cartFile.value;
  if (
    !cartValue.includes("<!-- Foxtrot same day delivery -->{% include 'foxtrot' %} <!-- Foxtrot same day delivery -->")
  ) {
    const indexOfTable = cartValue.indexOf('</table>');
    // Foxtrot Widget
    const widgetCode = "<!-- Foxtrot same day delivery -->{% include 'foxtrot' %} <!-- Foxtrot same day delivery -->";
    const newLiquid = cartValue.slice(0, indexOfTable + 8) + widgetCode + cartValue.slice(indexOfTable + 8);
    shopifyClient.asset.update(themeID, {
      key: 'sections/cart-template.liquid',
      value: newLiquid,
    });
  }
};

module.exports = { initShopifyClient, addWidget };
