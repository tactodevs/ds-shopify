const { models } = require('../database');
const logger = require('../../logger');

const addLocations = async payload => {
  try {
    await models.Location.bulkCreate(payload, {
      updateOnDuplicate: ['name', 'phone', 'address1', 'address2', 'city', 'province', 'provinceCode', 'countryCode', 'zip'],
    });
  } catch (error) {
    logger.error(error.message);
  }
};

const getLocation = async payload => {
  try {
    const location = await models.Location.findOne({ where: payload });
    return location;
  } catch (error) {
    logger.error(error);
  }
};

module.exports = { getLocation, addLocations };
