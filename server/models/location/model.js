const { DataTypes } = require('sequelize');

module.exports = database => {
  const Location = database.define('location', {
    name: {
      type: DataTypes.STRING,
    },
    phone: {
      type: DataTypes.STRING,
    },
    address1: {
      type: DataTypes.STRING,
    },
    address2: {
      type: DataTypes.STRING,
    },
    city: {
      type: DataTypes.STRING,
    },
    province: {
      type: DataTypes.STRING,
    },
    provinceCode: {
      type: DataTypes.STRING,
    },
    country: {
      type: DataTypes.STRING,
    },
    countryCode: {
      type: DataTypes.STRING,
    },
    zip: {
      type: DataTypes.STRING,
    },
    locationId: {
      type: DataTypes.BIGINT,
      unique: true,
      allowNull: false,
    },
  });
  return Location;
};
