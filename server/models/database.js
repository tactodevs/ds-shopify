const { DATABASE_HOST, DATABASE_USER, DATABASE, POSTGRES_PASSWORD } = process.env;
const Sequelize = require('sequelize');
const logger = require('../logger');
const Shop = require('./shop/model');
const Location = require('./location/model');
const EstimateHistory = require('./estimateHistory/model');
const Order = require('./order/model');
const ShippingAddress = require('./shippingAddress/model');

const sequilize = new Sequelize(DATABASE, DATABASE_USER, POSTGRES_PASSWORD, {
  host: `${DATABASE_HOST}`,
  dialect: 'postgres',
});

const initDatabase = async () => {
  try {
    await sequilize.authenticate();
    logger.info('DATABASE connection successfull.');
  } catch (error) {
    logger.error(`DATABASE connection failed.${error}`);
  }
};

const models = {
  Shop: Shop(sequilize),
  EstimateHistory: EstimateHistory(sequilize),
  Order: Order(sequilize),
  ShippingAddress: ShippingAddress(sequilize),
  Location: Location(sequilize),
};

Object.keys(models).forEach(modelName => {
  if ('associate' in models[modelName]) {
    models[modelName].associate(models);
  }
});

models.sequelize = sequilize;

module.exports = { models, initDatabase };
