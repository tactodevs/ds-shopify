/* eslint-disable no-console */
const winston = require('winston');
const transports = [];
const { format } = winston;
const isEmpty = require('lodash/isEmpty');
// When in development mode console log or else write the logs on file in staging or production mode.
if (process.env.NODE_ENV === 'development') {
  transports.push(
    new winston.transports.Console({
      format: format.combine(
        format.colorize({ all: true, colors: { error: 'red', info: 'yellow' } }),
        format.simple(),
        format.timestamp(),
      ),
    }),
  );
} else {
  transports.push(
    new winston.transports.File({
      filename: `./logs/${process.env.NODE_ENV}.log`,
      handleExceptions: true,
      json: true,
      maxsize: 5242880, // 5MB
      maxFiles: 5,
    }),
  );
}

const logger = winston.createLogger({ transports });

logger.stream = {
  write(message) {
    logger.info(message);
  },
};

const getErrorMessage = error => {
  if (error.response) {
    const { response } = error;
    if (!isEmpty(response.body)) {
      return response.body.errors.base[0];
    }
    if (!isEmpty(response.data)) {
      return response.data.errors[0].message;
    }
  }

  return error;
};

const logIt = {
  startLogging: app => {
    app.use(require('morgan')('combined', { stream: logger.stream }));
  },
  error: err => {
    logger.error(getErrorMessage(err));
  },
  info: info => {
    logger.info(info);
  },
};

module.exports = logIt;
