const ShopifyAPIClient = require('shopify-api-node');
const moment = require('moment');
const axios = require('axios');
const { isEmpty } = require('lodash');
const { DS_HOST, DS_API_KEY } = process.env;
const headers = { 'Content-Type': 'application/json', 'x-api-key': DS_API_KEY };
const { isNull } = require('lodash');

const sameDayDelivery = async ({ address, id, shop, accessToken, zipcode }) => {
  try {
    const shopifyClient = new ShopifyAPIClient({ shopName: shop, accessToken });

    // loop on each product
    const varIDs = await Promise.all(
      id.map(async varId => {
        const { inventory_item_id } = await shopifyClient.productVariant.get(varId);
        const data = await shopifyClient.inventoryLevel.list({ inventory_item_ids: inventory_item_id });
        const result = data.filter(res => res.available !== 0);
        // for each product- loop on locations
        const locationIds = await Promise.all(
          result.map(async res => {
            const { location_id } = res;
            const pickUpAddress = await shopifyClient.location.get(location_id);
            const addressStringified = JSON.stringify(address);
            const dropOffAddress = addressStringified.split(',');
            const dropOffDate = moment(Date.now()).format('YYYY-MM-DD');
            const dropOffTime = moment(Date.now()).format('hh:mm A');
            const value = moment(`${dropOffDate} ${dropOffTime}`, 'YYYY-M-D hh:mm A').valueOf();
            const dropTime = value.toString();

            const payload = {
              pickupAddress: {
                street: pickUpAddress.address1,
                street2: pickUpAddress.address2,
                city: pickUpAddress.city,
                state: pickUpAddress.province,
                zipcode: pickUpAddress.zip,
              },
              deliveryAddress: {
                street: dropOffAddress[0],
                street2: '',
                city: dropOffAddress[1],
                state: dropOffAddress[2],
                zipcode,
              },
              pickupTime: null,

              dropoffTime: {
                startsAt: dropTime,
                endsAt: parseFloat(dropTime) + 86400000,
              },
              packages: [
                {
                  name: 'small',
                  quantity: 1,
                  items: 1,
                },
              ],
              showEstimates: true,
              services: ['dsp', 'store-boundary', 'store-boundary-dsp'],
            };
            const url = `${DS_HOST}/api/v2/deliveryAssurance`;

            const { data: estimateResponse } = await axios({
              method: 'POST',
              url,
              data: JSON.stringify(payload),
              headers,
            });
            const estimates = estimateResponse.dsp && estimateResponse.dsp.estimates;

            if (!isEmpty(estimates)) {
              const todaysDate = moment(Date.now()).format('YYYY-MM-DD');
              const canDeliveryToday = estimates.filter(({ estimatedDeliveryTime }) => {
                const deliveryTime = moment(estimatedDeliveryTime).format('YYYY-MM-DD');
                return deliveryTime === todaysDate;
              });
              if (!isEmpty(canDeliveryToday)) {
                // Get the estimate which charges the least amount.
                const mapped = canDeliveryToday.reduce((first, second) => {
                  const final = first.amount < second.amount ? first : second;
                  return final;
                });
                if (!isNull(mapped)) {
                  return 1;
                }

                return 0;
              }
              return 0;
            }
            return 0;
          }),
        );
        if (locationIds.includes(1)) {
          return 1;
        }
        return 0;
      }),
    );
    if (!varIDs.includes(0)) {
      return true;
    }
  } catch (error) {
    console.log(error);
  }
  return false;
};

module.exports = { sameDayDelivery };
