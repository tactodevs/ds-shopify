const Router = require('koa-router');
const isEmpty = require('lodash/isEmpty');
const pick = require('lodash/pick');
const { receiveWebhook, registerWebhook } = require('@shopify/koa-shopify-webhooks');
const { ApiVersion } = require('@shopify/koa-shopify-graphql-proxy');
const ShopifyAPIClient = require('shopify-api-node');

const { getDeliveryAssurance } = require('../models/ds-api');
const logger = require('../logger');
const { addOrder, updateOrder, updateCanceledOrder } = require('../models/order');
const { getShop } = require('../models/shop');
const { getLocation, addLocations } = require('../models/location');
const { sameDayDelivery } = require('./widgetDeliveryAssurance');
const { isNull } = require('lodash');
const { cat } = require('shelljs');
const router = new Router({ prefix: '/webhook/' });

const { SHOPIFY_API_SECRET_KEY } = process.env;
const webhook = receiveWebhook({ secret: SHOPIFY_API_SECRET_KEY });

/**
 * This api is called from our widget on store front to check
 * if the same day delivery is available.
 */
router.post('sameday/delivery-storefront', async ctx => {
  const {
    request: { body, rawBody },
    query: { shop, signature },
  } = ctx;
  const { accessToken } = await getShop(shop);

  const { address, id, zipcode } = body;
  console.log(id);
  const canDelivery = await sameDayDelivery({ address, id, shop, accessToken, zipcode });
  console.log(canDelivery);
  ctx.status = 200;
  ctx.body = JSON.stringify({ canDelivery });
});

/**
 * This api is called on payment page after putting all the information.
 */
router.post('sameday/delivery/:shop', async ctx => {
  logger.info('Same day delivery webhook called on payment page');
  const { request: req, params } = ctx;
  const { shop } = params;
  const { body } = req;

  const { rate } = body;
  const { destination, origin, items, currency } = rate;

  const packages = items.map(item => ({ name: item.name, weight: item.grams / 453.59, quantity: item.quantity }));

  const payload = {
    showEstimates: true,
    services: ['dsp', 'store-boundary', 'store-boundary-dsp'],
    packages,
    pickupAddress: {
      street: origin.address1,
      street2: origin.address2,
      city: origin.city,
      country: origin.country,
      state: origin.province,
      zipcode: origin.postal_code,
    },
    deliveryAddress: {
      street: destination.address1,
      street2: destination.address2,
      city: destination.city,
      country: destination.country,
      state: destination.province,
      zipcode: destination.postal_code,
    },
  };
  const rates = await getDeliveryAssurance(payload, shop, currency);
  logger.info(payload);
  logger.info(shop);
  logger.info(currency);
  logger.info(rates);
  ctx.set('Content-Type', 'application/json');
  ctx.response.body = JSON.stringify(rates);
  logger.info('Rates given by getDeliveryAssurance');
});

// 1. Order Creation:
router.post('orders/create', webhook, async ctx => {
  logger.info('ORDER CREATION');
  const { request: req } = ctx;
  const { body } = req;
  const {
    id: shopifyOrderId,
    shipping_lines,
    shipping_address,
    name: orderName,
    total_price: totalCost,
    currency,
  } = body;
  const {
    zip,
    city,
    name,
    country,
    address1,
    address2,
    province,
    last_name,
    first_name,
    country_code,
    province_code,
  } = shipping_address;
  const shippingLine = !isEmpty(shipping_lines) ? shipping_lines[0] : {};
  const { price, phone, source, code = '' } = shippingLine;
  const myshopifyDomain = ctx.get('X-Shopify-Shop-Domain');

  const [
    pickupStreet = null,
    pickupStreet2 = null,
    pickupCity = null,
    pickupState = null,
    pickupCountry = null,
    pickupZipcode = null,
  ] = code.split('^');
  let location = await getLocation({
    myshopifyDomain,
    address1: pickupStreet,
    country: pickupCountry,
    provinceCode: pickupState,
    zip: pickupZipcode,
    city: pickupCity,
    address2: pickupStreet2 == 'null' ? null : pickupStreet2,
  });
  if (!location) {
    const { accessToken } = await getShop(myshopifyDomain);
    const shopifyClient = new ShopifyAPIClient({ shopName: myshopifyDomain, accessToken });
    const locations = await shopifyClient.location.list();
    const mappedLocations = locations.map(value => {
      value.countryCode = value.country_code;
      value.provinceCode = value.province_code;
      value.locationId = value.id;
      value.myshopifyDomain = myshopifyDomain;
      return pick(value, [
        'myshopifyDomain',
        'locationId',
        'countryCode',
        'provinceCode',
        'name',
        'address1',
        'address2',
        'city',
        'zip',
        'province',
        'country',
        'phone',
      ]);
    });
    await addLocations(mappedLocations);
    location = await getLocation({
      myshopifyDomain,
      address1: pickupStreet,
      country: pickupCountry,
      provinceCode: pickupState,
      zip: pickupZipcode,
      city: pickupCity,
      address2: pickupStreet2 == 'null' ? null : pickupStreet2,
    });
  }
  const customerName = name || `${first_name} ${last_name}`;

  const shippingAddress = {
    zip,
    city,
    name: customerName,
    phone,
    country,
    province,
    address1,
    address2,
    shopifyOrderId,
    countryCode: country_code,
    provinceCode: province_code,
  };
  if (source === 'foxtrot') {
    const payload = {
      shopifyOrderId,
      orderName,
      myshopifyDomain,
      estimatedCost: price,
      locationId: location.locationId,
      shippingAddress,
      totalCost,
      currency,
    };

    addOrder(payload);
    ctx.status = 200;
  }
});

// 2. Order Updation:
router.post('orders/updated', webhook, async ctx => {
  logger.info('ORDER UPDATED');
  ctx.status = 200;
});

// 3. Order Deletion:
router.post('app/uninstalled', webhook, async ctx => {
  logger.error('APP UNINSTALLED');
  ctx.status = 200;
});

// 4. Order Payment:
router.post('orders/payment', webhook, async ctx => {
  logger.error('ORDER PAYMENT');
  ctx.status = 200;
});

// 5. Order Fulfillment:
router.post('orders/fulfillment', webhook, async ctx => {
  logger.error('ORDER FULFILLMENT');
  ctx.status = 200;
});

// 6. Order Cancellation:
router.post('orders/cancelled', webhook, async ctx => {
  logger.info('ORDER CANCELLED');

  const { request: req } = ctx;
  const { body } = req;
  const { id } = body;

  const status = 'ORDER_CANCELLED';
  await updateCanceledOrder(status, id);
  logger.info('order cancelled!');

  ctx.status = 200;
});

router.post('locations/create', webhook, async ctx => {
  logger.info('Location Created');
  ctx.status = 200;
});

router.post('locations/update', webhook, async ctx => {
  logger.info('Location Updated');
  ctx.status = 200;
});

router.post('app/app_purchase_one_time_update', webhook, async ctx => {
  const { request: req, params } = ctx;
  const { shop } = params;
  const { body } = req;
  const { app_subscription } = body;
  console.log('\n\n\n app_purchase_one_time_update', JSON.stringify(app_subscription, null, 2), '\n\n\n');
  ctx.status = 200;
});

router.post('app/app_subscriptions_update', webhook, async ctx => {
  const { request: req, headers } = ctx;
  const { body } = req;
  const { app_subscription } = body;
  console.log('\n\n\n app_subscriptions_update', JSON.stringify(app_subscription, null, 2), '\n\n\n');
  ctx.status = 200;
});

// Delivery solutions order status update webhook.
router.post('ds/order_status/update', async ctx => {
  try {
    const { request: req } = ctx;
    const { body } = req;
    const { orderId, status } = body;

    const {
      order: { actualCost, shopifyOrderId },
      shop,
    } = await updateOrder(orderId, status);
    const { myshopifyDomain, accessToken, chargeId } = shop;

    if (
      status === 'ORDER_COMPLETED' ||
      status === 'ORDER_CANCELLED' ||
      status === 'ORDER_RETURNED' ||
      status === 'ORDER_UNDELIVERABLE'
    ) {
      const shopifyClient = new ShopifyAPIClient({ shopName: myshopifyDomain, accessToken });
      shopifyClient.usageCharge.create(chargeId, {
        price: actualCost,
        description: `Charge for order ${shopifyOrderId}`,
      });
    }
    ctx.status = 200;
  } catch (error) {
    logger.error(error);
  }
});

const registerWebhooks = async (shop, accessToken) => {
  const { APP_HOST } = process.env;
  const url = `${APP_HOST}/webhook/`;
  const creationWebhook = await registerWebhook({
    address: `${url}orders/create`,
    topic: 'ORDERS_CREATE',
    accessToken,
    shop,
    apiVersion: ApiVersion.July20,
  });
  if (!creationWebhook.success) {
    logger.error(creationWebhook.result);
  } else {
    logger.info('WEBHOOK ORDERS_CREATE');
  }
  const ordersCancelledWebhook = await registerWebhook({
    address: `${url}orders/cancelled`,
    topic: 'ORDERS_CANCELLED',
    accessToken,
    shop,
    apiVersion: ApiVersion.July20,
  });
  if (!ordersCancelledWebhook.success) {
    logger.error(ordersUpdatedWebhook.result);
  } else {
    logger.info('WEBHOOK ORDERS_CANCELLED');
  }
  const ordersUpdatedWebhook = await registerWebhook({
    address: `${url}orders/updated`,
    topic: 'ORDERS_UPDATED',
    accessToken,
    shop,
    apiVersion: ApiVersion.July20,
  });
  if (!ordersUpdatedWebhook.success) {
    logger.error(ordersUpdatedWebhook.result);
  } else {
    logger.info('WEBHOOK ORDERS_UPDATED');
  }

  // console.log(`${url}locations/create`);
  const locationCreationWebhook = await registerWebhook({
    address: `${url}locations/create`,
    topic: 'LOCATIONS_CREATE',
    accessToken,
    shop,
    apiVersion: ApiVersion.July19,
  });
  if (!locationCreationWebhook.success) {
    console.error(`ERROR :: ${JSON.stringify(locationCreationWebhook.result)}`);
  } else {
    logger.info('WEBHOOK LOCATIONS_CREATE');
  }

  const uninstalledWebhook = await registerWebhook({
    address: `${url}app/uninstalled`,
    topic: 'APP_UNINSTALLED',
    accessToken,
    shop,
    apiVersion: ApiVersion.July20,
  });

  if (!uninstalledWebhook.success) {
    logger.error(uninstalledWebhook.result);
  } else {
    logger.info('WEBHOOK APP_UNINSTALLED');
  }
  const appSubscription = await registerWebhook({
    address: `${url}app/app_purchase_one_time_update`,
    topic: 'APP_PURCHASES_ONE_TIME_UPDATE',
    accessToken,
    shop,
    apiVersion: ApiVersion.July20,
  });

  if (!appSubscription.success) {
    logger.error(appSubscription.result);
  } else {
    logger.info('WEBHOOK APP_PURCHASES_ONE_TIME_UPDATE');
  }

  const appSubscriptionUpdate = await registerWebhook({
    address: `${url}app/app_subscriptions_update`,
    topic: 'APP_SUBSCRIPTIONS_UPDATE',
    accessToken,
    shop,
    apiVersion: ApiVersion.July20,
  });

  if (!appSubscriptionUpdate.success) {
    logger.error(appSubscriptionUpdate.result);
  } else {
    logger.info('WEBHOOK APP_SUBSCRIPTIONS_UPDATE');
  }
  try {
    const callback_url = `${url}sameday/delivery/${shop}`;
    const shopifyClient = new ShopifyAPIClient({ shopName: shop, accessToken });

    await shopifyClient.carrierService.create({
      active: true,
      callback_url,
      carrier_service_type: 'api',
      name: 'foxtrot',
      service_discovery: true,
      format: 'json',
    });
  } catch (error) {
    logger.error('CARRIER SERVICE API IS ALREADY ADDED.');
  }
};
module.exports = { router, registerWebhooks };
