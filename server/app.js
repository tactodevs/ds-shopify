require('isomorphic-fetch');
const Koa = require('koa');
const { default: createShopifyAuth } = require('@shopify/koa-shopify-auth');
const { verifyRequest } = require('@shopify/koa-shopify-auth');
const ShopifyAPIClient = require('shopify-api-node');
const session = require('koa-session');
const { default: graphQLProxy, ApiVersion } = require('@shopify/koa-shopify-graphql-proxy');
const Router = require('koa-router');
const path = require('path');
const isEmpty = require('lodash/isEmpty');
const { ApolloServer } = require('apollo-server-koa');
const koaBody = require('koa-bodyparser');
const webpack = require('webpack');
const koaSend = require('koa-send');
const getSubscriptionUrl = require('./getSubscriptionUrl');
const logger = require('./logger');
const { router: webhooksRouter, registerWebhooks } = require('./webhooks');
const { initShopifyClient } = require('./models/shopify-apis');
const { models } = require('./models/database');

const resolvers = require('./apollo/resolvers');
const typeDefs = require('./apollo/typeDefs');

const port = parseInt(process.env.PORT, 10) || 3000;

const { SHOPIFY_API_SECRET_KEY, SHOPIFY_API_KEY } = process.env;

// serve files from webpack, in memory
const webpackMiddleware = async (server, router) => {
  if (process.env.NODE_ENV !== 'development') {
    const root = path.resolve(process.cwd(), 'build');
    router.get('*', verifyRequest(), async ctx => {
      let startPath = '/';
      let index = 'index.html';
      const ext = path.extname(ctx.path);
      if (!isEmpty(ext)) {
        startPath = ctx.path;
        index = ctx.path;
      }
      return koaSend(ctx, startPath, { index, root, gzip: true });
    });
  } else {
    const { devMiddleware, hotMiddleware } = require('koa-webpack-middleware');
    const config = require('../internals/webpack/webpack.dev.babel');
    const compiler = webpack(config);
    const middleware = devMiddleware(compiler, {
      logLevel: 'warn',
      silent: true,
      stats: 'errors-only',
      publicPath: '/assets/',
    });

    server.use(middleware);
    server.use(hotMiddleware(compiler));
    const { fileSystem } = middleware;

    router.get('*', verifyRequest(), async ctx => {
      const fileName = path.join(compiler.outputPath, 'index.html');
      ctx.response.type = 'html';
      const readStream = fileSystem.createReadStream(fileName);
      ctx.response.body = readStream;
    });
  }
  server.use(router.allowedMethods());
  server.use(router.routes());
};

const build = () => {
  const server = new Koa();
  const router = new Router();
  server.use(koaBody());
  server.use(session({ sameSite: 'none', secure: true }, server));
  server.keys = [SHOPIFY_API_SECRET_KEY];

  server.use(
    createShopifyAuth({
      apiKey: SHOPIFY_API_KEY,
      secret: SHOPIFY_API_SECRET_KEY,

      scopes: [
        'read_locations',
        'read_products',
        'read_inventory',
        'read_orders',
        'write_orders',
        'write_shipping',
        'write_inventory',
        'write_fulfillments',
        'write_products',
        'read_shipping',
        'read_themes',
        'write_themes',
        'write_customers',
        'write_checkouts',
        'read_fulfillments',
        'write_fulfillments',
        'read_third_party_fulfillment_orders',
        'write_third_party_fulfillment_orders',
        'read_assigned_fulfillment_orders',
        'write_assigned_fulfillment_orders',
        'read_merchant_managed_fulfillment_orders',
        'write_merchant_managed_fulfillment_orders',
      ],
      async afterAuth(ctx) {
        const { shop, accessToken } = ctx.session;
        ctx.cookies.set('shopOrigin', shop, {
          httpOnly: false,
          secure: true,
          sameSite: 'none',
        });
        initShopifyClient(shop, accessToken);
        registerWebhooks(shop, accessToken);
        const isProd = process.env.NODE_ENV !== 'development';

        if (isProd) {
          await getSubscriptionUrl(ctx, accessToken, shop);
        } else {
          ctx.redirect('/settings');
        }
      },
    }),
  );

  server.use(webhooksRouter.routes());
  server.use(webhooksRouter.allowedMethods());

  const apolloServer = new ApolloServer({
    debug: process.env.NODE_ENV !== 'production',
    typeDefs,
    resolvers,
    context: async ({ ctx: { session } }) => {
      const { shop, accessToken } = session;
      logger.info(`Access token:- ${accessToken}`);
      const shopifyClient = new ShopifyAPIClient({ shopName: shop, accessToken });

      return { shop, accessToken, models, shopifyClient };
    },
  });

  apolloServer.applyMiddleware({ app: server });

  server.use(graphQLProxy({ version: ApiVersion.July20 }));

  webpackMiddleware(server, router);

  server.listen(port, () => {
    logger.info(`> Ready on http://localhost:${port}`);
  });
};
build();
