/**
 * app.js
 *
 * This is the entry file for the application.
 */

import '@babel/polyfill';

// Import all the third party stuff
import React from 'react';
import ReactDOM from 'react-dom';
import { AppProvider } from '@shopify/polaris';
import App from 'containers/App';
import '@shopify/polaris/dist/styles.css';
import { ApolloProvider } from '@apollo/react-hooks';
import ApolloClient from 'apollo-boost';
import { Provider } from '@shopify/app-bridge-react';
import Cookies from 'js-cookie';
import en from '@shopify/polaris/locales/en.json';

import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

/* eslint-enable import/no-unresolved, import/extensions */

import MyRouter from './MyRouter';
import RoutePropagator from './RoutePropagator';
const MOUNT_NODE = document.getElementById('app');

const client = new ApolloClient({
  uri: '/graphql',
});

const render = () => {
  const { SHOPIFY_API_KEY } = process.env;
  const isProd = process.env.NODE_ENV !== 'development';
  const config = { apiKey: SHOPIFY_API_KEY, shopOrigin: Cookies.get('shopOrigin'), forceRedirect: isProd };

  ReactDOM.render(
    <ApolloProvider client={client}>
      <Provider config={config}>
        <AppProvider i18n={en}>
          <Router>
            <MyRouter />
            <RoutePropagator />
            <Switch>
              <Route path="/:type" component={App} />
              <Redirect to="/orders" />
            </Switch>
          </Router>
        </AppProvider>
      </Provider>
    </ApolloProvider>,
    MOUNT_NODE,
  );
};

if (module.hot) {
  // Hot reloadable React components and translation json files
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
  module.hot.accept(['containers/App'], () => {
    ReactDOM.unmountComponentAtNode(MOUNT_NODE);
    render();
  });
}

render();
