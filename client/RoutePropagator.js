import React from 'react';
import { withRouter } from 'react-router-dom';
import { RoutePropagator as AppBridgeRoutePropagator } from '@shopify/app-bridge-react';

const RoutePropagator = props => <AppBridgeRoutePropagator location={props.location} />;

export default withRouter(RoutePropagator);
