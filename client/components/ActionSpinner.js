import React from 'react';
import { Page, Card, Stack, Spinner } from '@shopify/polaris';

const ActionSpinner = () => (
  <Page title="Orders">
    <Card>
      <Stack distribution="center">
        <Spinner accessibilityLabel="Loading orders" size="large" color="teal" />
      </Stack>
    </Card>
  </Page>
);
export default ActionSpinner;
