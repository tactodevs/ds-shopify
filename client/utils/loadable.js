import React, { lazy, Suspense } from 'react';
import { Spinner, Stack, Card } from '@shopify/polaris';

/**
 * @param importFunc is function which returns the component which is to be loaded dynamucally,
 * and passed to lazy function of  react.
 * @param { fallback = null } is an object which will be passed to Suspense component of React
 * if fallback params is null then it will pick default value which is { fallback: null } after =
 * otherwise it will destructure and get fallback value from the passed param.
 *  This is how this component is suppose to be used.

    |--------------------------------------------------
    |
    |  export default loadable(() => import('./index'), {
    |    fallback: <ActivityIndicator visible />,
    |   });
    |
    |--------------------------------------------------
 *
 */
const loadable = importFunc => {
  const LazyComponent = lazy(importFunc);

  return props => (
    <Suspense
      fallback={
        <Card sectioned>
          <Stack distribution="center" alignment="center">
            <Spinner accessibilityLabel="Sample Spinner" size="large" color="teal" />
          </Stack>
        </Card>
      }
    >
      <LazyComponent {...props} />
    </Suspense>
  );
};

export default loadable;
