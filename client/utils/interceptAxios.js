import axios from 'axios';
import logout from 'api/logout';

// TODO: should also put logout function into saga system
export default (onLoggedOut, history) => {
  axios.interceptors.response.use(
    response => response,
    error => {
      const { status } = error.response;
      const userData = localStorage.getItem('USER_DATA');
      if (status === 403 && userData) {
        localStorage.clear();
        logout();
        onLoggedOut();
        history.replace('/login');
      }
      return Promise.reject(error);
    },
  );
};
