import React from 'react';
import { withRouter } from 'react-router-dom';
import { ClientRouter } from '@shopify/app-bridge-react';

const MyRouter = props => <ClientRouter history={props.history} />;

export default withRouter(MyRouter);
