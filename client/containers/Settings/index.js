/**
 *
 * OrderView
 *
 */

import React, { Fragment, useState, useCallback, useEffect } from 'react';
import {
  Card,
  Page,
  Frame,
  Stack,
  Toast,
  Layout,
  Banner,
  Button,
  Spinner,
  TextStyle,
  TextField,
  ChoiceList,
  SettingToggle,
} from '@shopify/polaris';
import styled from 'styled-components';
import Cookies from 'js-cookie';
import { useQuery, useMutation } from '@apollo/react-hooks';
import isNull from 'lodash/isNull';
import isEmpty from 'lodash/isEmpty';
import { SHOP_QUERY, UPDATE_SHOP } from './constants';

const Settings = () => {
  const myshopifyDomain = Cookies.get('shopOrigin');
  const { loading, data } = useQuery(SHOP_QUERY, { variables: { myshopifyDomain } });
  const [updateShop, { error }] = useMutation(UPDATE_SHOP);

  const [addOnCharge, setAddOnCharge] = useState(null);
  const handleAddOnAmountChange = useCallback(value => setAddOnCharge(value), []);

  const [maximumCharge, setMaximumCharge] = useState(null);
  const handleCapAmountChange = useCallback(value => setMaximumCharge(value), []);

  const [fixedCharge, setFixedCharge] = useState(null);
  const handleFixedChargeChange = useCallback(value => setFixedCharge(value), []);

  const [appEnabled, setAppEnabled] = useState(true);

  const [widgetEnabled, setWidgetEnabled] = useState(true);

  const [toastContent, setToastContent] = useState('');

  const [chargingMethod, setChargingMethod] = useState(['ADD_ON']);

  const handleChargingMethodChange = useCallback(value => setChargingMethod(value), []);

  const [deliveryTitle, setDeliveryTitle] = useState('');
  const handleDeliveryTitleChange = useCallback(value => setDeliveryTitle(value), []);

  const [apiKey, setAPIKey] = useState(null);
  const handleAPIChange = useCallback(value => setAPIKey(value), []);

  const toggleToast = useCallback(content => {
    if (!isEmpty(content)) {
      setToastContent(content);
    } else {
      setToastContent('');
    }
  }, []);

  const handleAppEnabledChange = useCallback(() => {
    updateShop({ variables: { myshopifyDomain, input: { appEnabled: !appEnabled } } });
    toggleToast(`The app has been ${appEnabled ? 'disabled' : 'enabled'}.`);
  }, [appEnabled, myshopifyDomain, toggleToast, updateShop]);

  const handleWidgetEnabledChange = useCallback(() => {
    updateShop({ variables: { myshopifyDomain, input: { widgetEnabled: !widgetEnabled } } });
    toggleToast(`The widget has been ${widgetEnabled ? 'disabled' : 'enabled'}.`);
  }, [widgetEnabled, myshopifyDomain, toggleToast, updateShop]);

  useEffect(() => {
    if (data && data.shop) {
      const {
        addOnCharge,
        maximumCharge,
        appEnabled,
        fixedCharge,
        chargingMethod,
        deliveryTitle,
        widgetEnabled,
        apiKey,
      } = data.shop;
      setAddOnCharge(`${addOnCharge}`);
      setMaximumCharge(`${maximumCharge}`);
      setAppEnabled(appEnabled);
      setChargingMethod([`${chargingMethod}`]);
      setFixedCharge(`${fixedCharge}`);
      setDeliveryTitle(`${deliveryTitle}`);
      setWidgetEnabled(widgetEnabled);
      setAPIKey(`${apiKey}`);
    }
  }, [data]);

  const handleSave = useCallback(() => {
    const input = {
      appEnabled,
      addOnCharge: parseFloat(addOnCharge || 0),
      maximumCharge: parseFloat(maximumCharge),
      fixedCharge: parseFloat(fixedCharge),
      chargingMethod: chargingMethod[0],
      deliveryTitle,
      apiKey,
    };

    updateShop({ variables: { myshopifyDomain, input } });
    toggleToast('Settings saved.');
  }, [
    appEnabled,
    addOnCharge,
    maximumCharge,
    fixedCharge,
    chargingMethod,
    deliveryTitle,
    apiKey,
    updateShop,
    myshopifyDomain,
    toggleToast,
  ]);

  const renderChild = useCallback(
    isSelected => {
      if (isSelected) {
        return (
          <TextField
            helpText="This amount will be charged to the customer if (estimate < fixed amount)"
            prefix="$"
            type="number"
            value={fixedCharge}
            onChange={handleFixedChargeChange}
            error={isValid(chargingMethod)}
          />
        );
      }
      return (
        <div style={{ marginTop: 15 }}>
          <div>
            <Stack>
              {/* <Card.Section> */}
              <div>
                <TextField
                  prefix="$"
                  type="number"
                  error={isValidMaximumCap()}
                  label="Cap Amount"
                  helpText={`If the estimate exceeds this amount then, ${deliveryTitle}`}
                  value={maximumCharge}
                  onChange={handleCapAmountChange}
                />
                <TextStyle variation="subdued"> option will not be visible to customer.</TextStyle>
              </div>
              <div style={{ marginLeft: 30 }}>
                <TextField
                  prefix="$"
                  label="Add On Amount"
                  type="number"
                  helpText="Estimate + Add on amount will be charged to the customer."
                  value={addOnCharge}
                  error={isValid(chargingMethod)}
                  onChange={handleAddOnAmountChange}
                />
              </div>
              {/* </Card.Section> */}
            </Stack>
          </div>
        </div>
        // <Fragment>
        //   <Card.Section>
        //     <TextField
        //       prefix="$"
        //       type="number"
        //       error={isValidMaximumCap()}
        //       label="Cap Amount"
        //       helpText={`If the estimate exceeds this amount then, ${deliveryTitle} option won't be visible to customer.`}
        //       value={maximumCharge}
        //       onChange={handleCapAmountChange}
        //     />
        //     <div style={{ marginTop: 10 }}>
        //       <TextField
        //         prefix="$"
        //         label="Add On Amount"
        //         type="number"
        //         helpText="Estimate + Add on amount will be charged to the customer."
        //         value={addOnCharge}
        //         error={isValid(chargingMethod)}
        //         onChange={handleAddOnAmountChange}
        //       />
        //     </div>
        //   </Card.Section>
        // </Fragment>
      );
    },
    [
      addOnCharge,
      chargingMethod,
      deliveryTitle,
      fixedCharge,
      handleAddOnAmountChange,
      handleCapAmountChange,
      handleFixedChargeChange,
      isValid,
      isValidMaximumCap,
      maximumCharge,
    ],
  );

  const isValid = useCallback(
    selected => {
      if (
        selected[0] === 'FIXED' &&
        (isEmpty(fixedCharge) || Math.sign(fixedCharge) === 0 || Math.sign(fixedCharge) === -1)
      ) {
        return 'Fixed amount should be positive decimal or number.';
      }
      if (selected[0] === 'ADD_ON' && isEmpty(addOnCharge)) {
        return 'Add on amount should be positive decimal or number.';
      }
      return null;
    },
    [addOnCharge, fixedCharge],
  );

  const isValidMaximumCap = useCallback(() => {
    if (isEmpty(maximumCharge) || Math.sign(maximumCharge) === -1 || Math.sign(maximumCharge) === 0) {
      return 'Maximum cap amount should be positive decimal or number.';
    }
    return null;
  });

  const renderSpinner = (
    <Card>
      <Stack distribution="center">
        <Spinner accessibilityLabel="Loading orders" size="large" color="teal" />
      </Stack>
    </Card>
  );

  if (error) {
    return (
      <Banner title="App crashed" status="critical">
        <p>The app has crashed due to some error.</p>
      </Banner>
    );
  }

  const toastMarkup = !isEmpty(toastContent) && (
    <div style={{ height: 12 }}>
      <Frame>
        <Toast content={toastContent} onDismiss={toggleToast} duration={2000} />
      </Frame>
    </div>
  );

  const textStatus = appEnabled ? 'enabled' : 'disabled';
  const contentStatus = appEnabled ? 'Disable' : 'Enable';
  const widgetStatus = widgetEnabled ? 'enabled' : 'disabled';
  const widgetContentStatus = widgetEnabled ? 'Disable' : 'Enable';
  const renderContent = (
    <Fragment>
      <Card>
        <SettingToggle
          action={{
            content: contentStatus,
            onAction: handleAppEnabledChange,
          }}
          enabled={appEnabled}
        >
          Foxtrot is <TextStyle variation="strong">{textStatus}</TextStyle>.
        </SettingToggle>
      </Card>
      <Card>
        <SettingToggle
          action={{
            content: widgetContentStatus,
            onAction: handleWidgetEnabledChange,
          }}
          enabled={widgetEnabled}
        >
          Foxtrot Widget is {widgetEnabled}
          <TextStyle variation="strong">{widgetStatus}</TextStyle>.
        </SettingToggle>
        <Card.Section>
          <TextField
            // error={isValidMaximumCap()}
            label="Google Places API key for widget."
            helpText="This API key will be used for the widget on the cart page."
            value={apiKey}
            onChange={handleAPIChange}
          />
        </Card.Section>
      </Card>
      <Card>
        <Card.Section>
          <TextField
            // error={isValidMaximumCap()}
            label="Shipment Title"
            helpText="This title will be visible for Shipment Method"
            value={deliveryTitle}
            onChange={handleDeliveryTitleChange}
          />
        </Card.Section>
      </Card>
      <Card>
        <Card.Section>
          <Stack>
            <ChoiceList
              choices={[
                { label: 'Variable charge', value: 'ADD_ON' },
                { label: 'Fixed charge', value: 'FIXED', renderChildren: renderChild },
              ]}
              selected={chargingMethod}
              onChange={handleChargingMethodChange}
            />
          </Stack>
        </Card.Section>
      </Card>

      <SaveButton>
        <Stack spacing="extraLoose" distribution="trailing" alignment="trailing">
          <Button disabled={isValid(chargingMethod) || isValidMaximumCap()} onClick={handleSave}>
            Save
          </Button>
        </Stack>
      </SaveButton>
    </Fragment>
  );
  return (
    <Page title="Settings" fullWidth>
      <Layout>
        <Layout.Section>
          {loading || isNull(maximumCharge) || isNull(addOnCharge) ? renderSpinner : renderContent}
        </Layout.Section>
        {toastMarkup}
      </Layout>
    </Page>
  );
};

const SaveButton = styled.div`
  margin-top: 16px;
`;
export default Settings;
