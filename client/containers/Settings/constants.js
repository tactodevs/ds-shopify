/*
 *
 * OrderView constants
 *
 */
import { gql } from 'apollo-boost';

export const DEFAULT_ACTION = 'client/OrderView/DEFAULT_ACTION';

const SHOP_QUERY = gql`
  query Shop($myshopifyDomain: String!) {
    shop(myshopifyDomain: $myshopifyDomain) {
      name
      id
      shopId
      maximumCharge
      addOnCharge
      fixedCharge
      chargingMethod
      myshopifyDomain
      appEnabled
      widgetEnabled
      deliveryTitle
      apiKey
    }
  }
`;

const UPDATE_SHOP = gql`
  mutation UpdateShop($myshopifyDomain: String!, $input: ShopInput) {
    updateShop(myshopifyDomain: $myshopifyDomain, input: $input) {
      id
      maximumCharge
      fixedCharge
      chargingMethod
      addOnCharge
      appEnabled
      widgetEnabled
      deliveryTitle
      apiKey
    }
  }
`;

export { SHOP_QUERY, UPDATE_SHOP };
