/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect, useCallback } from 'react';
import '../../global-styles';
import { Page, Link, FooterHelp, Tabs } from '@shopify/polaris';
import Orders from 'containers/Orders/Loadable';
import Settings from 'containers/Settings/Loadable';
import styled from 'styled-components';
import { Route, Redirect, Switch } from 'react-router-dom';
const App = ({ history, match: { params } }) => {
  const { type } = params;
  const [selected, setSelected] = useState(0);
  const handleTabChange = useCallback(
    index => {
      let url = '';
      if (index === 0) {
        url = '/orders';
      }
      if (index === 1) {
        url = '/settings';
      }
      setSelected(index);
      history.replace(url);
    },
    [history],
  );
  useEffect(() => {
    if (type === 'orders') {
      setSelected(0);
    }
    if (type === 'settings') {
      setSelected(1);
    }
  }, [type]);
  const tabs = [
    {
      id: 'orders',
      content: 'Orders',
      panelID: 'orders',
    },
    {
      id: 'settings',
      content: 'Settings',
      panelID: 'settings',
    },
  ];
  return (
    <Page fullWidth>
      <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange} />
      <Switch>
        <Route path="/orders" component={Orders} />
        <Route path="/settings" component={Settings} />
        <Redirect to="/orders" />
      </Switch>
      <CustBanner>
        <FooterHelp title="Contact for support">
          Contact us
          <Link url="https://help.foxtrot.com/manual" external>
            {' Foxtrot Support'}
          </Link>
        </FooterHelp>
      </CustBanner>
    </Page>
  );
};

const CustBanner = styled.div`
  margin-left: 40px;
  margin-right: 4.2%;
  margin-left: 4.2%;
`;
export default App;
