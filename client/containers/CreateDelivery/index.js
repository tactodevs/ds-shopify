/**
 *
 * DashBoard
 *
 */

import React, { useState, useCallback } from 'react';
import { Card, Page, Select, Layout, Modal, Stack, Button } from '@shopify/polaris';

import styled from 'styled-components';
import Estimate from '../Estimate';

const CreateDelivery = ({ history, match }) => {
  const { params } = match;
  const { orderId } = params;
  const [showCreateEstimate, setShowCreateEstimate] = useState(false);

  const [checked, setChecked] = useState(false);
  const handleChanges = useCallback(newChecked => setChecked(newChecked), []);

  const options = [
    { label: 'Andheri', value: 'today' },
    { label: 'Bhayandar', value: 'yesterday' },
    { label: 'Churchgate', value: 'lastWeek' },
  ];

  return (
    <Page title="Create New Delivery">
      <Layout>
        <Layout.Section oneThird>
          <Card title="Order Details">
            <Card.Section title="Shopify Order Id">
              <p>002299887654</p>
            </Card.Section>
          </Card>
        </Layout.Section>
        <Layout.Section oneThird>
          <Card title="Package Details">
            <Card.Section title="Total value">
              <p>INR - 25000.00</p>
            </Card.Section>
          </Card>
        </Layout.Section>
        <Layout.Section oneThird>
          <Card>
            <Card.Section title="Pick-Up details">
              <Select label="Pickup Location Name" options={options} />
            </Card.Section>
          </Card>
        </Layout.Section>

        <Layout.Section secondary>
          <Card title="Shipping Details">
            <Card.Section>
              <Subinfo>Alifiya KhAlifan</Subinfo>
              <p>CustID: 0099008877665</p>
            </Card.Section>
            <Card.Section title="Default Information">
              <Subinfo>Alifiya Khan</Subinfo>
              <p>CustID : 0099008877665</p>
              <p>Signature : Tacto Infomedia</p>
              <p>
                PhoneNo :<span>+91 8080878685</span>
              </p>
              <p>
                Delivery Instructions : <span> Delivery Instructions</span>
              </p>
            </Card.Section>
            <Card.Section title="Drop-off Location">
              <p>
                Street : <span>Navghar Road</span>
              </p>
              <p>
                Country : <span>India</span>
              </p>
              <p>
                State : <span>Maharashtra</span>
              </p>
              <p>
                City : <span>Mumbai</span>
              </p>
              <p>
                Pincode : <span>400081</span>
              </p>
            </Card.Section>
          </Card>
        </Layout.Section>
        <Layout.Section oneHalf>
          <Stack>
            <Button destructive onClick={history.goBack}>
              Cancel Delivery
            </Button>
            <Button
              primary
              onClick={() => {
                setShowCreateEstimate(!showCreateEstimate);
              }}
            >
              Get Estimate
            </Button>
          </Stack>
        </Layout.Section>
      </Layout>
      <Modal
        title="Estimate"
        open={showCreateEstimate}
        primaryAction={{
          content: 'Place order',
        }}
        secondaryActions={[
          {
            content: 'Cancel',
            onAction: () => {
              setShowCreateEstimate(false);
            },
          },
        ]}
        onClose={() => setShowCreateEstimate(false)}
      >
        <Modal.Section>
          <Estimate />
        </Modal.Section>
      </Modal>
    </Page>
  );
};

const DateWrapper = styled.div`
  padding: 20px 0px 10px 0px;
`;
const Text = styled.div`
  font-size: 20px;
  padding-bottom: 20px;
`;
const Subdata = styled.div`
  font-size: 16px;
  padding: 0px 20px;
  margin-bottom: 10px;
`;

const Subinfo = styled.div`
  font-size: 16px;
  padding: 4px 0px;
  font-weight: bold;
`;
export default CreateDelivery;
