/**
 *
 * OrderView
 *
//  */

import React from 'react';
import { Card, Page, Banner } from '@shopify/polaris';
import styled from 'styled-components';
import { ShipmentMajorMonotone } from '@shopify/polaris-icons';
// import { Header } from '@shopify/polaris/types/latest/src/components/Card/components';

const Estimate = () => (
  <Page>
    <div>
      <Card title="Roadie" sectioned>
        <Info>
          <Estimateinfo>
            <Maintitle>
              <span>Delivery Estimate Cost</span>
            </Maintitle>
            <Subinfo>
              <p>INR 100.00</p>
            </Subinfo>
          </Estimateinfo>
          <Estimateinfo>
            <Maintitle>
              <span>Estimate Expiry Time</span>
            </Maintitle>
            <Subinfo>
              <p>May 21st 2020, 1.00 pm IST</p>
            </Subinfo>
          </Estimateinfo>
          <Estimateinfo>
            <Maintitle>
              <span>Requested Pickup Time</span>
            </Maintitle>
            <Subinfo>
              <p>May 21st 2020, 1.00 pm IST</p>
            </Subinfo>
          </Estimateinfo>
          <Estimateinfo>
            <Maintitle>
              <span>Estimate Delivery Time</span>
            </Maintitle>
            <Subinfo>
              <p> Until May 21st 2020, 1.00 pm IST</p>
            </Subinfo>
          </Estimateinfo>
          <Estimateinfo>
            <Maintitle>
              <span>Requested Delivery Time</span>
            </Maintitle>
            <Subinfo>
              <p>May 21st 2020, 1.00 pm IST</p>
            </Subinfo>
          </Estimateinfo>
        </Info>

        <Banner title="Placing The Order" status="info">
          <p>Placing the order will incur a charge of INR 1000.</p>
        </Banner>
      </Card>
    </div>
  </Page>
);

const Headingele = styled.div`
  padding: 20px;
  display: flex;
  @media screen and (max-width: 576px) {
    flex-direction: column;
  }
`;
const Text = styled.div`
  font-size: 20px;
  padding: 0px 0px 0px 10px;
  font-weight: 500;
`;

const Info = styled.div`
  background: #ddd;
  padding: 20px;
  border-radius: 2px;
`;
const Estimateinfo = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px;
`;

const Subinfo = styled.p`
  text-align: left;
  width: 50%;
  font-weight: 500;
`;
const Maintitle = styled.span`
  font-weight: 600 !important;
  font-size: 14px;
`;
export default Estimate;
