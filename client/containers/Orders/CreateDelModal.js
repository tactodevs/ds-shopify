/* eslint-disable no-underscore-dangle */
import React, { useEffect, useState, useCallback } from 'react';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import {
  Modal,
  Button,
  Collapsible,
  Popover,
  ResourceList,
  ChoiceList,
  Layout,
  Select,
  DatePicker,
  Card,
  TextStyle,
  Stack,
  RadioButton,
} from '@shopify/polaris';
import isEmpty from 'lodash/isEmpty';
import { ArrowDownMinor, ArrowUpMinor } from '@shopify/polaris-icons';
import moment from 'moment';
import { GET_ESTIMATE, CREATE_DELIVERY, timeList, signatureValues } from './constants';
import AddressLayoutSection from './AddressLayoutSection';
const CreateDelModal = ({ setToast, history, handleModalClose, order, shopifyOrderId, setErrorToast }) => {
  const [isSpirit, setIsSpirit] = useState('spiritNo');
  const handleNoSpirit = useCallback((checked, newValue) => {
    setIsSpirit(newValue);
    //  console.log(_checked, newValue);
  }, []);

  const [isBeerOrWine, setIsBeerOrWine] = useState('beerNo');
  const handleIsBeerOrWine = useCallback((_checked, newValue) => setIsBeerOrWine(newValue), []);

  const [hasPerishableItems, setHasPerishableItems] = useState('perishableNo');
  const handlePerishableChange = useCallback((_checked, newValue) => setHasPerishableItems(newValue), []);

  const [signature, setSignature] = useState('not_required');
  const handleSignatureChange = useCallback(value => setSignature(value), []);

  const [isTobacco, setIsTobacco] = useState('tobaccoNo');
  const handleIsTobacco = useCallback((_checked, newValue) => setIsTobacco(newValue), []);

  const [isFragile, setIsFragile] = useState('fragileNo');
  const handleIsFragile = useCallback((_checked, newValue) => setIsFragile(newValue), []);

  const [pickupTime, setPickupTime] = useState(['PICKUP_ASAP']);
  const handlePickUpTime = useCallback(value => {
    setPickupTime(value);
    setDropOffDate(moment(Date.now()).format('YYYY-MM-DD'));
    setDropOffTime(moment(Date.now()).format('hh:mm A'));
  }, []);

  const [active, setActive] = useState(false);
  const handleToggle = useCallback(() => setActive(active => !active), []);

  const [popoverActive, setPopoverActive] = useState(false);
  const togglePopoverActive = useCallback(() => setPopoverActive(popoverActive => !popoverActive), []);
  const [timePopoverActive, setTimePopoverActive] = useState(false);

  const [{ month, year }, setDate] = useState({ month: moment().month(), year: moment().year() });
  const [selectedDates, setSelectedDates] = useState({ start: new Date(), end: new Date() });

  const [dropOffDate, setDropOffDate] = useState(moment(Date.now()).format('YYYY-MM-DD'));
  const [dropOffTime, setDropOffTime] = useState(moment(Date.now()).format('hh:mm A'));

  const handleSelectedDates = useCallback(
    value => {
      const date = moment(value.start).format('YYYY-MM-DD');
      setDropOffDate(date);
      setSelectedDates(value);
      togglePopoverActive(false);
      setTimePopoverActive(true);
    },
    [togglePopoverActive],
  );

  const handleMonthChange = useCallback((month, year) => {
    setDate({ month, year });
  }, []);

  const [
    fetchEstimate,
    { error, data: { getEstimate } = { getEstimate: { _id: null } }, loading: fetchingEstimate },
  ] = useLazyQuery(GET_ESTIMATE);

  const [createDeliveryApi, { loading }] = useMutation(CREATE_DELIVERY, { errorPolicy: 'all' });

  const createDelivery = useCallback(async () => {
    try {
      const estimateId = getEstimate._id;
      const variables = { shopifyOrderId, estimateId };
      await createDeliveryApi({ variables });
      setToast(`Delivery created for the order ${order.orderName}`);
      history.replace('/orders');
    } catch (error) {
      const errorData = JSON.parse(JSON.stringify(error));
      console.log(errorData);
      setErrorToast(`Delivery not created. ${error.graphQLErrors[0].message}`);
      history.replace('/orders');
    }
  }, [createDeliveryApi, getEstimate._id, history, order.orderName, setErrorToast, setToast, shopifyOrderId]);
  const value = moment(`${dropOffDate} ${dropOffTime}`, 'YYYY-M-D hh:mm A').valueOf();

  useEffect(() => {
    const variables = {
      shopifyOrderId,
      input: {
        isSpirit: isSpirit === 'spiritYes',
        isBeerOrWine: isBeerOrWine === 'beerYes',
        isTobacco: isTobacco === 'tobaccoYes',
        isFragile: isFragile === 'fragileYes',
        dropOffTime: `${value.toString()}`,
        signature,
        hasPerishableItems: hasPerishableItems === 'perishableYes',
      },
    };

    fetchEstimate({ variables });
  }, [
    fetchEstimate,
    hasPerishableItems,
    isBeerOrWine,
    isFragile,
    isSpirit,
    isTobacco,
    shopifyOrderId,
    signature,
    value,
  ]);
  const noEstimateFound = () => error || isEmpty(getEstimate);

  const renderEstimate = () => {
    if (error || isEmpty(getEstimate) || isEmpty(getEstimate)) {
      return <TextStyle variation="negative">No estimate available for this order.</TextStyle>;
    }
    return (
      <TextStyle variation="positive">{`You will be charged $${parseFloat(
        getEstimate.amount,
      )} for this delivery`}</TextStyle>
    );
  };

  const activator = (
    <Button fullWidth onClick={togglePopoverActive} disclosure>
      {`${dropOffDate}, ${dropOffTime}`}
    </Button>
  );

  const renderTimeItem = value => (
    <Button
      monochrome
      fullWidth
      size="slim"
      onClick={() => {
        setDropOffTime(`${value}`);
        setTimePopoverActive(false);
      }}
    >
      {value}
    </Button>
  );
  const [visibleTimeIndex, setVisibleTimeIndex] = useState(5);

  const handleScrolledToBottom = useCallback(() => {
    const totalIndexes = timeList.length;
    const interval = visibleTimeIndex + 3 < totalIndexes ? 3 : totalIndexes - visibleTimeIndex;

    if (interval > 0) {
      setVisibleTimeIndex(visibleTimeIndex + interval);
    }
  }, [visibleTimeIndex]);

  const renderDatePicker = selected => {
    if (!selected) {
      return null;
    }
    return (
      <Card.Section fullWidth>
        <Popover sectioned active={popoverActive} activator={activator} onClose={togglePopoverActive}>
          <DatePicker
            disableDatesBefore={
              new Date(
                moment()
                  .subtract(1, 'day')
                  .format(),
              )
            }
            month={month}
            year={year}
            onChange={handleSelectedDates}
            onMonthChange={handleMonthChange}
            selected={selectedDates}
            allowRange={false}
          />
        </Popover>

        <Popover
          sectioned
          active={timePopoverActive}
          activator={<div />}
          onClose={togglePopoverActive}
          ariaHaspopup={false}
        >
          <Popover.Pane onScrolledToBottom={handleScrolledToBottom}>
            <ResourceList items={timeList} renderItem={renderTimeItem} />
          </Popover.Pane>
        </Popover>
      </Card.Section>
    );
  };
  const renderContent = () => {
    const { shippingAddress, location } = order;

    return (
      <Layout>
        <AddressLayoutSection title="Shipping From" address={location} />
        <AddressLayoutSection title="Shipping To" address={shippingAddress} />
        <Layout.Section oneHalf>
          <Card>
            <Card.Section>
              <Button
                size="slim"
                fullWidth
                icon={active ? ArrowUpMinor : ArrowDownMinor}
                ariaExpanded={active}
                onClick={handleToggle}
                ariaControls="basic-collapsible"
              >
                Additional Information
              </Button>
              <Collapsible
                open={active}
                id="basic-collapsible"
                transition={{ duration: '150ms', timingFunction: 'ease' }}
              >
                <Card.Section>
                  <ChoiceList
                    title="Time Window in which the package will be ready to be picked up."
                    choices={[
                      { label: 'As soon as possible', value: 'PICKUP_ASAP' },
                      { label: 'Custom', value: 'PICKUP_CUSTOM', renderChildren: renderDatePicker },
                    ]}
                    selected={pickupTime}
                    onChange={handlePickUpTime}
                  />
                </Card.Section>
                <Card.Section>
                  Delivery Contains Spirits?
                  <Stack>
                    <RadioButton
                      label="Yes"
                      checked={isSpirit === 'spiritYes'}
                      onChange={handleNoSpirit}
                      id="spiritYes"
                    />
                    <RadioButton label="No" checked={isSpirit === 'spiritNo'} onChange={handleNoSpirit} id="spiritNo" />
                  </Stack>
                </Card.Section>

                <Card.Section>
                  Delivery Contains Beer or Wine?
                  <Stack>
                    <RadioButton
                      label="Yes"
                      onChange={handleIsBeerOrWine}
                      checked={isBeerOrWine === 'beerYes'}
                      id="beerYes"
                    />
                    <RadioButton
                      label="No"
                      onChange={handleIsBeerOrWine}
                      checked={isBeerOrWine === 'beerNo'}
                      id="beerNo"
                    />
                  </Stack>
                </Card.Section>

                <Card.Section>
                  Delivery Contains Tobacco?
                  <Stack>
                    <RadioButton
                      label="Yes"
                      onChange={handleIsTobacco}
                      checked={isTobacco === 'tobaccoYes'}
                      id="tobaccoYes"
                    />
                    <RadioButton
                      label="No"
                      onChange={handleIsTobacco}
                      checked={isTobacco === 'tobaccoNo'}
                      id="tobaccoNo"
                    />
                  </Stack>
                </Card.Section>

                <Card.Section>
                  Delivery has Fragile Items?
                  <Stack>
                    <RadioButton
                      label="Yes"
                      onChange={handleIsFragile}
                      checked={isFragile === 'fragileYes'}
                      id="fragileYes"
                    />
                    <RadioButton
                      label="No"
                      onChange={handleIsFragile}
                      checked={isFragile === 'fragileNo'}
                      id="fragileNo"
                    />
                  </Stack>
                </Card.Section>

                <Card.Section>
                  Delivery has perishable items?
                  <Stack>
                    <RadioButton
                      label="Yes"
                      onChange={handlePerishableChange}
                      checked={hasPerishableItems === 'perishableYes'}
                      id="perishableYes"
                    />
                    <RadioButton
                      label="No"
                      onChange={handlePerishableChange}
                      checked={hasPerishableItems === 'perishableNo'}
                      id="perishableNo"
                    />
                  </Stack>
                </Card.Section>

                <Card.Section>
                  <Select
                    label="Signature"
                    options={signatureValues}
                    onChange={handleSignatureChange}
                    value={signature}
                  />
                </Card.Section>
              </Collapsible>
            </Card.Section>
          </Card>
        </Layout.Section>
      </Layout>
    );
  };

  return (
    <Modal
      open
      footer={!fetchingEstimate && renderEstimate()}
      onClose={handleModalClose}
      title={`Create delivery for ${order.orderName}`}
      primaryAction={
        !noEstimateFound() && {
          loading: fetchingEstimate || loading,
          content: 'Create delivery',
          onAction: createDelivery,
        }
      }
      secondaryActions={[
        {
          content: noEstimateFound() ? 'Ok' : 'Cancel',
          onAction: handleModalClose,
        },
      ]}
    >
      <Modal.Section>{renderContent()}</Modal.Section>
    </Modal>
  );
};
export default CreateDelModal;
