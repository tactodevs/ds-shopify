/**
 *
 * DashBoard
 *
 */

import React, { useState, useCallback } from 'react';
import {
  Card,
  Page,
  Toast,
  Stack,
  Badge,
  Banner,
  Frame,
  Button,
  Filters,
  Spinner,
  DataTable,
  Pagination,
  EmptyState,
} from '@shopify/polaris';
import { useQuery } from '@apollo/react-hooks';
import isEmpty from 'lodash/isEmpty';
import isNull from 'lodash/isNull';
import Cookies from 'js-cookie';

import { empty } from 'apollo-boost';
import { size } from 'lodash';
import { ORDERS } from './constants';

import CreateDelivery from './CreateDelivery';
import ShowModal from './ShowModal';
import CancelModal from './CancelModal';

const Orders = ({ history, location }) => {
  const values = [
    'ORDER_DELAYED',
    'ORDER_ASSIGNED',
    'PICKUP_STARTED',
    'ORDER_RETURNED',
    'ORDER_CONFIRMED',
    'ORDER_DELIVERED',
    'ORDER_CANCELLED',
    'ERROR_EXCEPTION',
    'PICKUP_COMPLETED',
    'PICKUP_EXCEPTION',
    'OUT_FOR_DELIVERY',
    'ORDER_REDELIVERY',
    'ORDER_UNDELIVERABLE',
  ];

  const query = new URLSearchParams(location.search);
  const shopifyOrderId = query.get('id');
  const dsOrderId = query.get('dsOrderId');
  const shopOrigin = Cookies.get('shopOrigin');
  const [offset, setOffset] = useState(0);

  const [toast, setToast] = useState(null);
  const [errorToast, setErrorToast] = useState(null);
  const [dspDetails, setDSPDetails] = useState(null);
  const [shopifyId, setShopifyId] = useState(null);
  // const [cancelOrder, setCancelOrder] = useState(null);
  const handleDSPDetailsChange = useCallback(value => () => setDSPDetails(value), []);
  // const handleCancelChange = useCallback(value => () => setCancelOrder(value), []);

  const [queryValue, setQueryValue] = useState('');

  const handleFiltersQueryChange = useCallback(value => setQueryValue(value), []);
  const handleQueryValueRemove = useCallback(() => setQueryValue(null), []);

  const { loading, error, data = { orders: { rows: [], count: 0 } } } = useQuery(ORDERS, {
    variables: { offset, searchTerm: queryValue },
  });
  const {
    orders: { count, rows },
  } = data;
  const items = { rows };

  const appliedFilters = [];

  const filters = [];

  if (error) {
    return (
      <Banner title="App crashed" status="critical">
        <p>The app has crashed </p>
      </Banner>
    );
  }

  const renderStatus = (status, id) => {
    if (isEmpty(status)) {
      return (
        <div>
          <Button
            primary
            onClick={() => {
              history.replace(`/orders?id=${id}`);
            }}
          >
            {status || 'Dispatch'}
          </Button>
        </div>
      );
    }
    if (status === 'ORDER_CANCELLED') {
      return <Badge>{status}</Badge>;
    }
    if (status === 'ORDER_DELIVERED') return <Badge progress="complete">{status}</Badge>;
    return (
      <div>
        <Badge>{status}</Badge>
      </div>
    );
  };

  const renderDsp = (dsp, dsTrackingNumber, dsTrackingUrl) => {
    const activator = (
      <Button plain onClick={handleDSPDetailsChange(dsp)}>
        {dsp}
      </Button>
    );
    return (
      <div>
        {activator}
        <div>
          <a href={dsTrackingUrl} target="_blank">
            {dsTrackingNumber}
          </a>
        </div>
      </div>
    );
  };
  const renderCancel = (status, dsOrderId, shopifyId) => {
    if (!isEmpty(status) && !values.includes(status)) {
      return (
        <Button
          destructive
          onClick={() => {
            history.replace(`/orders?dsOrderId=${dsOrderId}`);
            setShopifyId(shopifyId);
          }}
        >
          Cancel
        </Button>
      );
    }
    if (isEmpty(status)) {
      return <Badge>N/A</Badge>;
    }
    if (status === 'ORDER_CANCELLED') return <Badge>Cancelled</Badge>;
    return (
      <div>
        <Badge>N/A</Badge>
      </div>
    );
  };
  const renderActualCost = actualCost => {
    if (!isNull(actualCost)) {
      return (
        <div>
          ${actualCost}
          <p style={{ fontSize: 12 }}>+$1 Foxtrot charge</p>
        </div>
      );
    }
    return '';
  };
  const mappedRows = items.rows.map(item => {
    const {
      orderName,
      dsTrackingNumber,
      dsOrderId,
      location,
      actualCost,
      estimatedCost,
      dsOrderStatus,
      dsTrackingUrl,
      dspName,
    } = item;
    return [
      <a href={`https://${shopOrigin}/admin/orders/${item.shopifyOrderId}`} target="_blank">
        {orderName}
      </a>,
      renderDsp(dspName, dsTrackingNumber, dsTrackingUrl),
      // <a href={dsTrackingUrl} target="_blank">
      //   {dsTrackingNumber}
      // </a>,
      `${location.name},${location.province}`,
      `$${estimatedCost}`,
      renderActualCost(actualCost),
      // `${!isNull(actualCost) ? `$${actualCost} ` : ''}`,
      renderStatus(dsOrderStatus, item.shopifyOrderId, dsOrderId),
      renderCancel(dsOrderStatus, dsOrderId, item.shopifyOrderId, dspName),
    ];
  });

  const hasNext = offset < count / 25 - 1;
  const hasPrevious = offset > 0;

  const toastMarkup = !isEmpty(toast) && (
    <div style={{ height: 12 }}>
      <Frame>
        <Toast content={toast} duration={2000} onDismiss={setToast} />
      </Frame>
    </div>
  );

  const errorToastMarkup = !isEmpty(errorToast) && (
    <div style={{ height: 12 }}>
      <Frame>
        <Toast error content={errorToast} duration={2000} onDismiss={setErrorToast} />
      </Frame>
    </div>
  );

  return (
    <Page title="Orders" fullWidth>
      <Card>
        <Card.Section>
          <Filters
            queryValue={queryValue}
            filters={filters}
            appliedFilters={appliedFilters}
            onQueryChange={handleFiltersQueryChange}
            onQueryClear={handleQueryValueRemove}
          />
        </Card.Section>
        {isEmpty(mappedRows) ? (
          <EmptyState
            heading="Order list is empty."
            image="https://cdn.shopify.com/s/files/1/0262/4071/2726/files/emptystate-files.png"
          />
        ) : (
          <>
            {loading ? (
              <Card.Section fullWidth>
                <Stack distribution="center" alignment="center">
                  <Spinner />
                </Stack>
              </Card.Section>
            ) : (
              <DataTable
                columnContentTypes={['text', 'text', 'text', 'text', 'text', 'text']}
                headings={[
                  'Order ID',
                  'Delivery Partner & URL',
                  'Location',
                  'Cost to customer',
                  'Actual cost',
                  'Status',
                  'Cancel Order',
                ]}
                rows={mappedRows}
              />
            )}
          </>
        )}
      </Card>
      <div style={{ paddingTop: 20 }}>
        <Stack distribution="center">
          <Pagination
            hasPrevious={hasPrevious}
            hasNext={hasNext}
            onPrevious={() => setOffset(offset - 1)}
            onNext={() => setOffset(offset + 1)}
          />
        </Stack>
      </div>

      {shopifyOrderId && (
        <CreateDelivery
          shopifyOrderId={shopifyOrderId}
          setToast={setToast}
          history={history}
          setErrorToast={setErrorToast}
        />
      )}
      {dsOrderId && (
        <CancelModal
          dsOrderId={dsOrderId}
          setToast={setToast}
          history={history}
          setErrorToast={setErrorToast}
          shopifyId={shopifyId}
        />
      )}
      {toastMarkup}
      {errorToastMarkup}
      {dspDetails && <ShowModal dspDetails={dspDetails} onDismiss={handleDSPDetailsChange(null)} />}
    </Page>
  );
};
export default Orders;
