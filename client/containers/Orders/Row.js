import React from 'react';

import { Button, ResourceItem, Badge, TextStyle } from '@shopify/polaris';
import styled from 'styled-components';
import isEmpty from 'lodash/isEmpty';

const Row = ({ item, shopOrigin, history }) => {
  const {
    id,
    dspName,
    location,
    orderName,
    customerName,
    dsOrderStatus,
    estimatedCost,
    shopifyOrderId,
    dsTrackingNumber,
    dsTrackingUrl,
  } = item;
  const { city, province, zip } = location;

  return (
    [`${orderName}`,`${dspName}`,`${dsTrackingNumber}`,`${`${city} ${province}, ${zip}`}`,`${estimatedCost}`,`${dsOrderStatus}`]
    // <ResourceItem id={`${id}-${dsOrderStatus}`} accessibilityLabel={`View details for ${orderName}`}>
    //   <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
    //     <Name>
    //       <TextStyle variation="strong">
    //         <a href={`https://${shopOrigin}/admin/orders/${shopifyOrderId}`} target="_blank">
    //           {orderName}
    //         </a>
    //       </TextStyle>
    //       <TextStyle variation="subdued">{customerName}</TextStyle>
    //     </Name>
    //     <div>{dspName}</div>
    //     <div>
    //       <a href={dsTrackingUrl} target="_blank">
    //         {dsTrackingNumber}
    //       </a>
    //     </div>
    //     <div>{`${city} ${province}, ${zip}`}</div>
    //     <TextStyle variation="strong">${estimatedCost}</TextStyle>
    //     {isEmpty(dsOrderStatus) ? (
    //       <Button primary onClick={() => history.push(`orders/${shopifyOrderId}`)}>
    //         {dsOrderStatus || 'Dispatch'}
    //       </Button>
    //     ) : (
    //       <Badge>{dsOrderStatus}</Badge>
    //     )}
    //   </div>
    // </ResourceItem>
  );
};

const Name = styled.div`
  display: flex;
  flex-direction: column;
`;

export default Row;
