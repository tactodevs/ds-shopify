import React, { useCallback } from 'react';
import { Modal, TextContainer } from '@shopify/polaris';

const ShowModal = ({ dspDetails, onDismiss }) => {
  console.log(dspDetails);
  const handleChange = useCallback(() => {
    onDismiss();
  }, [onDismiss]);

  return (
    <div style={{ height: '500px' }}>
      <Modal
        open
        onClose={handleChange}
        title={`${dspDetails === 'DoorDash' ? 'Doordash' : 'Postmates'} Details`}
        primaryAction={{
          content: 'Ok',
          onAction: handleChange,
        }}
      >
        <Modal.Section>
          <TextContainer>
            {dspDetails === 'DoorDash' && (
              <div>
                <p>Contact Details: support@doordash.com / 855-973-1040</p>
              </div>
            )}
            {dspDetails === 'Postmates' && (
              <div>
                <p>Contact Details: help@postmates.com / 800-882-6106</p>
              </div>
            )}
          </TextContainer>
        </Modal.Section>
      </Modal>
    </div>
  );
};
export default ShowModal;
