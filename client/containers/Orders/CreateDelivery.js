/* eslint-disable no-underscore-dangle */
import React, { useCallback, useState } from 'react';
import { Toast, Frame, Modal, Banner } from '@shopify/polaris';
import { useQuery } from '@apollo/react-hooks';
import isNull from 'lodash/isNull';
import isEmpty from 'lodash/isEmpty';
import { ORDER } from './constants';
import CreateDelModal from './CreateDelModal';
import OrderDispatchedModal from './OrderDispatchedModal';

const CreateDelivery = ({ setToast, shopifyOrderId, history, setErrorToast }) => {
  const { loading, data: { order } = { order }, error } = useQuery(ORDER, {
    variables: { shopifyOrderId: `${shopifyOrderId}` },
  });

  const [toastActive, setToastActive] = useState(false);

  const toggleActive = useCallback(() => setToastActive(toastActive => !toastActive), []);

  const handleModalClose = useCallback(() => {
    history.replace('/orders');
  }, [history]);

  if (loading || !isEmpty(error) || isEmpty(order)) {
    return (
      <Modal title="Loading..." open loading={loading}>
        <Modal.Section>
          <Banner status="critical">
            <p>Something went worng.</p>
          </Banner>
        </Modal.Section>
      </Modal>
    );
  }

  if (order && isNull(order.dsOrderStatus)) {
    return (
      <CreateDelModal
        shopifyOrderId={shopifyOrderId}
        handleModalClose={handleModalClose}
        order={order}
        setToast={setToast}
        setErrorToast={setErrorToast}
        history={history}
      />
    );
  }

  if (order && order.dsOrderStatus) {
    return <OrderDispatchedModal handleModalClose={handleModalClose} order={order} />;
  }
  return (
    <div style={{ height: 12 }}>
      <Frame>
        <Toast error content="Something went Wrong!" onDismiss={toggleActive} />
      </Frame>
    </div>
  );
};

export default CreateDelivery;
