import React from 'react';
import { Modal, TextContainer } from '@shopify/polaris';

const OrderDispatchedModal = ({ order, handleModalClose }) => {
  const { actualCost } = order;
  return (
    <Modal
      title="Delivery details"
      open
      onClose={handleModalClose}
      secondaryActions={[
        {
          content: 'Close',
          onAction: handleModalClose,
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <h3>{`Cost of your order was $${actualCost}`}</h3>
          <p>Press Close to Return</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  );
};
export default OrderDispatchedModal;
