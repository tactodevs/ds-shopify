import React from 'react';
import { TextStyle, Layout, Card } from '@shopify/polaris';
import isEmpty from 'lodash/isEmpty';
const AddressLayoutSection = ({ title, address }) => {
  if (isEmpty(address)) {
    return <div />;
  }
  const { name, phone, address1, address2, city, province, country, zip } = address;
  return (
    <Layout.Section oneThird>
      <Card title={title}>
        <Card.Section>
          <TextStyle variation="subdued">
            <p>
              <span>{name}</span>
            </p>
            <p>
              <span>
                {address1} {address2}
              </span>
            </p>

            <p>
              <span>
                {city} {province} {zip}
              </span>
            </p>

            <p>
              <span>{country}</span>
            </p>
            <p>
              <span>{phone}</span>
            </p>
          </TextStyle>
        </Card.Section>
      </Card>
    </Layout.Section>
  );
};

export default AddressLayoutSection;
