/*
 *
 * Orders constants
 *
 */

import { gql } from 'apollo-boost';

const GET_ESTIMATE = gql`
  query GetEstimate($shopifyOrderId: String!, $input: EstimateInput!) {
    getEstimate(shopifyOrderId: $shopifyOrderId, input: $input) {
      amount
      estimatedDeliveryTime
      _id
    }
  }
`;

const CREATE_DELIVERY = gql`
  mutation CreateDelivery($estimateId: String!, $shopifyOrderId: String!) {
    createDelivery(estimateId: $estimateId, shopifyOrderId: $shopifyOrderId) {
      id
      orderName
      actualCost
      dsTrackingUrl
      dsOrderId
      dsTrackingNumber
      dspName
      estimatedCost
      deliveredAt
      dsOrderStatus
      createdAt
      updatedAt
      myshopifyDomain
      shopifyOrderId
      shippingAddress {
        name
        phone
        address1
        address2
        city
        province
        provinceCode
        country
        countryCode
        zip
      }
      location {
        name
        phone
        address1
        address2
        city
        province
        provinceCode
        country
        countryCode
        zip
      }
    }
  }
`;
const CANCEL_DELIVERY = gql`
  mutation CancelDelivery($dsOrderId: String!, $shopifyOrderId: String!, $checked: Boolean!, $cancelReason: String!) {
    cancelDelivery(
      dsOrderId: $dsOrderId
      shopifyOrderId: $shopifyOrderId
      checked: $checked
      cancelReason: $cancelReason
    ) {
      id
      orderName
      actualCost
      dsTrackingUrl
      dsOrderId
      dsTrackingNumber
      dspName
      estimatedCost
      deliveredAt
      dsOrderStatus
      createdAt
      updatedAt
      myshopifyDomain
      shopifyOrderId
      shippingAddress {
        name
        phone
        address1
        address2
        city
        province
        provinceCode
        country
        countryCode
        zip
      }
      location {
        name
        phone
        address1
        address2
        city
        province
        provinceCode
        country
        countryCode
        zip
      }
    }
  }
`;
const ORDERS = gql`
  query GetEstimate($limit: Int! = 25, $offset: Int!, $searchTerm: String) {
    orders(limit: $limit, offset: $offset, searchTerm: $searchTerm) {
      count
      rows {
        id
        dspName
        dsTrackingNumber
        dsTrackingUrl
        dsOrderId
        orderName
        actualCost
        estimatedCost
        deliveredAt
        dsOrderStatus
        createdAt
        updatedAt
        myshopifyDomain
        shopifyOrderId
        shippingAddress {
          name
          phone
          address1
          address2
          city
          province
          provinceCode
          country
          countryCode
          zip
        }
        location {
          name
          phone
          address1
          address2
          city
          province
          provinceCode
          country
          countryCode
          zip
        }
      }
    }
  }
`;

const ORDER = gql`
  query Order($shopifyOrderId: String!) {
    order(shopifyOrderId: $shopifyOrderId) {
      id
      orderName
      actualCost
      estimatedCost
      deliveredAt
      dsOrderStatus
      createdAt
      updatedAt
      myshopifyDomain
      shopifyOrderId
      dsOrderId
      shippingAddress {
        name
        phone
        address1
        address2
        city
        province
        provinceCode
        country
        countryCode
        zip
      }
      location {
        name
        phone
        address1
        address2
        city
        province
        provinceCode
        country
        countryCode
        zip
      }
    }
  }
`;

const timeList = [
  '12:00 AM',
  '12:30 AM',
  '1:00 AM',
  '1:30 AM',
  '2:00 AM',
  '2:30 AM',
  '3:00 AM',
  '3:30 AM',
  '4:00 AM',
  '4:30 AM',
  '5:00 AM',
  '5:30 AM',
  '6:00 AM',
  '6:30 AM',
  '7:00 AM',
  '7:30 AM',
  '8:00 AM',
  '8:30 AM',
  '9:00 AM',
  '9:30 AM',
  '10:00 AM',
  '10:30 AM',
  '11:00 AM',
  '11:30 AM',
  '12:00 PM',
  '12:30 PM',
  '1:00 PM',
  '1:30 PM',
  '2:00 PM',
  '2:30 PM',
  '3:00 PM',
  '3:30 PM',
  '4:00 PM',
  '4:30 PM',
  '5:00 PM',
  '5:30 PM',
  '6:00 PM',
  '6:30 PM',
  '7:00 PM',
  '7:30 PM',
  '8:00 PM',
  '8:30 PM',
  '9:00 PM',
  '9:30 PM',
  '10:00 PM',
  '10:30 PM',
  '11:00 PM',
  '11:30 PM',
];
const signatureValues = [
  { label: 'Not Required', value: 'not_required' },
  { label: 'Required', value: 'required' },
  { label: 'Recipient Required', value: 'required_recipient' },
  { label: 'Unattended', value: 'unattended' },
];

export { ORDERS, CREATE_DELIVERY, GET_ESTIMATE, ORDER, timeList, CANCEL_DELIVERY, signatureValues };
