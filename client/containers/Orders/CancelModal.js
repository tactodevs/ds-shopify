/* eslint-disable no-underscore-dangle */
import React, { useCallback, useState } from 'react';
import { Modal, Card, Select, Checkbox, Subheading } from '@shopify/polaris';
import { useMutation } from '@apollo/react-hooks';
import { CANCEL_DELIVERY } from './constants';

const CancelModal = ({ dsOrderId, history, setErrorToast, setToast, shopifyId: shopifyOrderId }) => {
  const [cancelDeliveryApi, { loading }] = useMutation(CANCEL_DELIVERY, { errorPolicy: 'all' });
  const orderCancelDetail = [
    { label: 'Customer changed/cancelled order', value: 'customer' },
    { label: 'Fraudulent Order', value: 'fraud' },
    { label: 'Items Unavailable', value: 'inventory' },
    { label: 'Payment declined', value: 'declined' },
    { label: 'Other', value: 'other' },
  ];

  const [cancelReason, setCancelReason] = useState('other');
  const handleReasonChange = useCallback(value => setCancelReason(value), []);
  const [checked, setChecked] = useState(false);
  const handleChange = () => setChecked(!checked);

  const cancelDelivery = useCallback(async () => {
    try {
      const variables = { dsOrderId, shopifyOrderId, checked, cancelReason };
      await cancelDeliveryApi({ variables });
      // console.log(response);
      setToast('Delivery Cancelled');
      history.replace('/orders');
    } catch (error) {
      setErrorToast(`Delivery can not be cancelled`);
      history.replace('/orders');
    }
  }, [cancelDeliveryApi, cancelReason, checked, dsOrderId, history, setErrorToast, setToast, shopifyOrderId]);
  const handleModalClose = useCallback(() => {
    history.replace('/orders');
  }, [history]);
  return (
    <Modal
      open
      onClose={handleModalClose}
      title="Cancel Order"
      primaryAction={{
        content: 'Keep Order',
        onAction: handleModalClose,
      }}
      secondaryActions={[
        {
          loading,
          content: 'Cancel Order',
          onAction: cancelDelivery,
        },
      ]}
    >
      <Modal.Section>
        <Card>
          <Card.Section>
            <Subheading>Inventory</Subheading>
            <Checkbox label="Restock items" checked={checked} onChange={handleChange} />
          </Card.Section>
          <Card.Section>
            <Subheading>Reason for cancelation</Subheading>
            <Select options={orderCancelDetail} onChange={handleReasonChange} value={cancelReason} />
          </Card.Section>
        </Card>
      </Modal.Section>
    </Modal>
  );
};

export default CancelModal;
