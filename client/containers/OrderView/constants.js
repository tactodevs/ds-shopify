/*
 *
 * OrderView constants
 *
 */
import gql from 'graphql-tag';

export const DEFAULT_ACTION = 'client/OrderView/DEFAULT_ACTION';

const ORDER_QUERY = id => gql`{
  order(shopifyOrderId: "${id}") {
    name
    dsOrderId
    shopId
    shopifyOrderId
    shippingAddress {
      country
      provinceCode
      zipcode
      city
      country
      address1
      address2
    }
  }
  }`;

export { ORDER_QUERY };
