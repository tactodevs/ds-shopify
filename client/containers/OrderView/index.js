/**
 *
 * OrderView
 *
 */

import React from 'react';
import { Card, Layout, Page, Icon, Badge } from '@shopify/polaris';
import styled from 'styled-components';
import {
  CancelSmallMinor,
  OrdersMajorMonotone,
  PackageMajorMonotone,
  DraftOrdersMajorMonotone,
  ShipmentMajorMonotone,
} from '@shopify/polaris-icons';
import { Link, Redirect } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import isEmpty from 'lodash/isEmpty';
import { ORDER_QUERY } from './constants';
const OrderView = ({ location }) => {
  const query = new URLSearchParams(location.search);
  const orderId = query.get('id');
  const { loading, error, data } = useQuery(ORDER_QUERY(orderId));

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;
  const { order } = data;
  const { dsOrderId } = order || {};

  if (isEmpty(dsOrderId)) {
    return <Redirect to={`/order/createdelivery/${orderId}`} />;
  }

  return (
    <Page title="View  Order" breadcrumbs={[{ content: <Link to="/orders">Orders</Link> }]}>
      {/* <ButtonWrapper>
      <Button>Back</Button>
    </ButtonWrapper> */}
      <Layout>
        <Layout.Section>
          <Card>
            <Headingele>
              <div>
                <Icon source={OrdersMajorMonotone} />
              </div>
              <div>
                <Text>Order Details</Text>
              </div>
            </Headingele>
            <Card.Section>
              <p>DHL0008877665</p>
              <Orderinfo>
                <p>Order Status</p>
                <Badge>Estimate Recieved</Badge>
              </Orderinfo>
              <Orderinfo>
                <p>ASAP Delivery</p>
                <Badge status="critical">
                  <Icon source={CancelSmallMinor} />
                </Badge>
              </Orderinfo>
              <Orderinfo>
                <p>Store</p>
                <Badge>0000</Badge>
              </Orderinfo>
              <Orderinfo>
                <p>Department</p>
                <Badge>Fruits</Badge>
              </Orderinfo>
              <Orderinfo>
                <p>Package value</p>
                <Badge>00.00</Badge>
              </Orderinfo>
            </Card.Section>
          </Card>
          <Card>
            <Headingele>
              <div>
                <Icon source={PackageMajorMonotone} />
              </div>
              <div>
                <Text>Package Details</Text>
              </div>
            </Headingele>
            <Card.Section>
              <Orderinfo>
                <p>Size</p>
                <Badge>100 in. X 100 in . X 100 in.</Badge>
              </Orderinfo>
              <Orderinfo>
                <p>Weight</p>
                <Badge>100 kg</Badge>
              </Orderinfo>
              <Orderinfo>
                <p>Quantity</p>
                <Badge>100</Badge>
              </Orderinfo>
              <Orderinfo>
                <p>Items</p>
                <Badge>100</Badge>
              </Orderinfo>
            </Card.Section>
          </Card>
          {/* <Card>
            <Headingele>
              <div>
                <Icon source={DraftOrdersMajorMonotone} />
              </div>
              <div>
                <Text>Drop-off Deatils</Text>
              </div>
            </Headingele>
            <Card.Section>
              <p>Amazon prime (store Name)</p>
              <p>
                Global Business Park, <br />
                Street 57
                <br /> Delhi <br />
                HR1222339900
              </p>
            </Card.Section>
            <Card.Section>
              <p>Customer ID :</p>
              <p>Cust 000887766</p> <br />
              <p>Phone Number :</p>
              <p>+91 8087868545</p> <br />
              <p>Start Time</p>
              <p> 20 may 2020, 10.00 am IST</p>
              <br />
              <p>End Time</p>
              <p> 20 may 2020, 10.00 am IST</p>
              <br />
              <p>Pick Up Instructions:</p>
              <p>N/A</p>
            </Card.Section>
          </Card> */}
        </Layout.Section>
        <Layout.Section secondary>
          <Card>
            <Headingele>
              <div>
                <Icon source={ShipmentMajorMonotone} />
              </div>
              <div>
                <Text>Pickup Deatils</Text>
              </div>
            </Headingele>
            <Card.Section>
              <p>Amazon prime (store Name)</p>
              <p>+91 8087868545</p> <br />
              <p>
                Global Business Park, <br />
                Street 57
                <br /> Delhi <br />
                HR1222339900
              </p>
            </Card.Section>
            {/* <Card.Section>
              <p>Phone Number :</p>
              <p>+91 8087868545</p> <br />
              <p>Start Time</p>
              <p> 20 may 2020, 10.00 am IST</p>
              <br />
              <p>End Time</p>
              <p> 20 may 2020, 10.00 am IST</p>
              <br />
              <p>Pick Up Instructions:</p>
              <p>N/A</p>
            </Card.Section> */}
          </Card>
          <Card>
            <Headingele>
              <div>
                <Icon source={DraftOrdersMajorMonotone} />
              </div>
              <div>
                <Text>Drop-off Deatils</Text>
              </div>
            </Headingele>
            <Card.Section>
              <p>Alifiya Khan (Customer Name)</p>
              <p>+91 8087868545</p>
              <p>CustId: 000887766</p> <br />
              <p>
                Global Business Park, <br />
                Street 57
                <br />
                Delhi <br />
                HR1222339900
              </p>
            </Card.Section>
            {/* <Card.Section>
              <p>Customer ID :</p>
              <p>Cust 000887766</p> <br />
              <p>Phone Number :</p>
              <p>+91 8087868545</p> <br />
              <p>Start Time</p>
              <p> 20 may 2020, 10.00 am IST</p>
              <br />
              <p>End Time</p>
              <p> 20 may 2020, 10.00 am IST</p>
              <br />
              <p>Pick Up Instructions:</p>
              <p>N/A</p>
            </Card.Section> */}
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  );
};
const Headingele = styled.div`
  padding: 20px 20px 0px 20px;
  display: flex;
  backgroud-color: red;
`;
const Text = styled.div`
  font-size: 16px;
  padding: 0px 0px 0px 10px;
  font-weight: 500;
`;
const ButtonWrapper = styled.div`
  padding: 10px 0px 20px 0px;
  display: flex;
  justify-content: flex-end;
`;
const Orderinfo = styled.div`
  display: flex;
  padding: 8px 0px 0px 0px;
  justify-content: space-between;
`;

export default OrderView;
