module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        modules: false,
      },
    ],
    '@babel/preset-react',
  ],
  plugins: ['styled-components', '@babel/plugin-proposal-class-properties', '@babel/plugin-syntax-dynamic-import'],
  env: {
    production: {
      only: ['client'],
      plugins: ['lodash', 'transform-react-remove-prop-types', '@babel/plugin-transform-react-inline-elements'],
    },
    test: {
      plugins: ['@babel/plugin-transform-runtime', '@babel/plugin-transform-modules-commonjs', 'dynamic-import-node'],
    },
  },
};
