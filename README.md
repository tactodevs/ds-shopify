# Foxtrot - Development Installation Instructions

## Local Environment Setup

### Install Docker:

To make sure whether Docker is installed or not:

1. **RUN** `docker -v` in your terminal.

If Docker is not installed, you can download and install [Docker for mac here.](https://docs.docker.com/docker-for-mac/install/)

### Getting the Foxtrot App up on local:

1. First clone Foxtrot app on your system **RUN** `git clone git@bitbucket.org:tactodevs/ds-shopify.git` in your terminal.

2. Now we need to build our docker images and containers. But before that we will have to share our Foxtrot repository directory with docker using file sharing option in docker. To share directories with docker navigate to docker's file sharing documentation [Docker File Sharing](https://docs.docker.com/docker-for-mac/#file-sharing) after sharing the directory your docker.

3. Once you are done sharing the directory with docker we can now build docker images go inside the repository and **RUN** `docker-compose -f docker-compose.dev.yml up --build` in your terminal.

4. This will take a while to **build**.

5. To run the application on local we need to shell into the `web container` for that open new terminal and run **RUN** `docker exec -it foxtrot bash`This will shell you into the container.

6. Now that you are inside the web container **RUN** `npm install` to install the npm dependencies.

7. After all the dependencies are installed **RUN** `npm start` to start the app.

8. The Foxtrot application should be up and available on `http://localhost:3000/`

9. Now our application is up and running but our database schema is not created yet. We are using postgres and sequilize(ORM) to communicate with out databse.
10. Using sequilize we can create our initial schema using sync({force:true}) function from sequilize [Sequilize Sync](https://sequelize.org/master/manual/model-basics.html#model-synchronization)
11. Inside our database.js call `sequilize.sync({ force:true })` once the database has been authenticated.
12. After that nodemon will restart our app and the schema will be created.

_**Note:** You only have to build the Docker Image, import the database and install the npm dependencies initially. After it's been built successfully you can start it again by doing the following steps:_

---

### To boot up your newly built Docker Image:

1. Navigate to the Foxtrot's repository

2. **RUN** `docker-compose -f docker-compose.dev.yml up` in your terminal.

3. Open a new terminal and shell into web container **RUN** `docker exec -it foxtrot bash`

4. Start the web app **RUN** `npm start`

_**Note:** Once Docker is up and running you can then develop on your local and it will auto compile and hot reload with every change you make on your local. Sometimes docker fails to detect the changes done on the host machine and auto compilation stops to work in that scenario try to restart your machine or try the following steps:_

---

### Shut Down Foxtrot on Docker:

Depending on the circumstances needed to shut Docker down will dictate how to properly bring Docker down.

1. To bring Docker down **gracefully** make sure you're on the terminal window with Docker running and **TYPE** `ctrl + C`.

For major changes to your codebase, you'll need to bring Docker down via compose before any images can be removed from Docker. Make sure you're on the terminal window with Docker running:

2. **RUN** `docker-compose down`.

_**Note:** This will bring all your running containers down from Docker._

---

### Remove Docker Images:

To remove docker images make sure you have brought your Docker containers down or you will not be able to remove any images.

1. **RUN** `docker images` in your terminal.

This will list all your current images with their unique ID's on Docker. Then to remove an image:

2. **RUN** `docker rmi -f <imageID>` in your terminal.

_**Note:** Once you have removed an image you will need to rebuild Docker again to view any major code updates on your local._

---

## Staging deplyoment Setup

On our ubuntu server docker is already installed and setup and is running.

1. To see the running containers run `docker ps` it will list our all the currectly running containers on our server.
2. To rebuild our foxtrot app we will have to shell into our container using `docker exec -it foxtrot bash`.
3. Once you are in just run `npm start build:staging`.
4. Voila!!! have a cup of black coffee and enjoy the app running.
5. Same steps for production environment.

### Footnotes

1. You can shell into our database container using `docker exec -it postgres-foxtrot bash` and manipulate database using psql commands.
2. `.env-cmdrc.json` file contains all the env variables which are being used inside our app.
3. Any changes insider `.env-cmdrc.json` will require you to restart our container.
