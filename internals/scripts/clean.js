const shell = require('shelljs');
const addCheckMark = require('./helpers/checkmark.js');

if (!shell.test('-e', 'internals/templates')) {
  shell.echo('The example is deleted already.');
  shell.exit(1);
}

process.stdout.write('Cleanup started...');

shell.mv(
  'client/containers/LanguageProvider/tests',
  'internals/templates/containers/LanguageProvider',
);

// Cleanup components/
shell.rm('-rf', 'client/components/*');

// Handle containers/
shell.rm('-rf', 'client/containers');
shell.mv('internals/templates/containers', 'client');

// Handle tests/
shell.mv('internals/templates/tests', 'client');

// Handle translations/
shell.rm('-rf', 'client/translations');
shell.mv('internals/templates/translations', 'client');

// Handle utils/
shell.rm('-rf', 'client/utils');
shell.mv('internals/templates/utils', 'client');

// Replace the files in the root client/ folder
shell.cp('internals/templates/app.js', 'client/app.js');
shell.cp('internals/templates/global-styles.js', 'client/global-styles.js');
shell.cp('internals/templates/index.html', 'client/index.html');

// Remove the templates folder
shell.rm('-rf', 'internals/templates');

addCheckMark();

shell.echo('\nCleanup done. Happy Coding!!!');
